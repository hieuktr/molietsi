
import { REHYDRATE } from 'redux-persist/constants';
import { DEFAULT_BUCKET, DEFAULT_REGION, DEFAULT_ACCESS_KEY, DEFAULT_SECRET_KEY, DEFAULT_CDN_URL } from '../constants';
import { 
  SET_TOKEN,
  SET_PROFILE,
  SET_AUTH_STATUS,
  SET_UPDATE_STATUS,
  SET_SYNC_STATUS,
  ADD_SYNC_STATUS_CONTENT,
  SET_LAST_UPDATED_AT,
  
  SET_CONFIG,
  SET_BUCKET,
  SET_CDN_URL,
  
  SET_CEMETERIES,
  SET_TOMBS_BY_CEMETERY,
  
  SET_CAPTURED,
  UPDATE_IDS_CAPTURED,
  
  SET_CEMETERY_CAPTURED,
  
  SET_NEW_TOMBS,
  SET_UPDATE_TOMBS,
  SET_UPDATE_CEMETERY,
  
  REMOVE_NEW_TOMBS,
  REMOVE_UPDATE_TOMBS,
  REMOVE_UPDATE_CEMETERIES,
  
  RESET_TOMBS,
  RESET_STATE,
} from '../actions/user';

const initialState = {
  token: '',
  profile: {},
  authStatus: '',
  authError: '',
  uploadStatus: {},
  syncStatus: {
    content: '',
    isFinish: true,
    fail: 0,
    success: 0,
    total: 0
  },
  updateStatus: {
    required: false,
    link: '',
    version: '',
    build: 0
  },
  cemeteries: [],
  tombsByCemetery: {},
  totalTombs: 0,
  lastUpdatedAt : {},
  captured: {},
  cemeteryCaptured: {},
  newTombs: {},
  updateTombs : {},
  updateCemeteries : {},
  config: {
    bucket: DEFAULT_BUCKET,
    region: DEFAULT_REGION,
    accessKey: DEFAULT_ACCESS_KEY,
    secretKey: DEFAULT_SECRET_KEY,
    cdnUrl: DEFAULT_CDN_URL,
  },
  bucket : DEFAULT_BUCKET,
  cdnUrl : DEFAULT_CDN_URL,
};

export default function (state = initialState, action) {
  if (action.type === SET_TOKEN) {
    return {
      ...state,
      token: action.token,
    };
  }

  if (action.type === SET_PROFILE) {
    return {
      ...state,
      profile: action.profile,
    };
  }
  
  if (action.type === SET_CONFIG) {
    return {
      ...state,
      config: action.config,
    };
  }
  
  if (action.type === SET_BUCKET) {
    return {
      ...state,
      bucket: action.bucket,
    };
  }
  
  if (action.type === SET_CDN_URL) {
    return {
      ...state,
      cdnUrl: action.cdnUrl,
    };
  }

  if (action.type === SET_AUTH_STATUS) {
    return {
      ...state,
      authStatus: action.status,
      authError: action.error,
    };
  }
  
  if (action.type === SET_SYNC_STATUS) {
    return {
      ...state,
      syncStatus: {
        isFinish: action.syncStatus.isFinish,
        content: action.syncStatus.content,
        fail: action.syncStatus.fail || 0,
        success: action.syncStatus.success || 0,
        total: action.syncStatus.total || 0,
      },
    };
  }
  
  if (action.type === ADD_SYNC_STATUS_CONTENT) {
    return {
      ...state,
      syncStatus: {
        ...state.syncStatus,
        isFinish: action.isFinish,
        content: state.syncStatus.content.replace('...', '') + '\n' + action.content + (action.isFinish ? '' : '...'),
        total: action.total || state.syncStatus.total
      }
    };
  }

  if (action.type === SET_UPDATE_STATUS) {
    return {
      ...state,
      updateStatus: action.status,
    };
  }
  
  if (action.type === SET_LAST_UPDATED_AT) {
    return {
      ...state,
      lastUpdatedAt: {
        ...state.lastUpdatedAt,
        [action.cemeteryId]: action.time
      },
    };
  }
  
  if (action.type === SET_CEMETERIES) {
    return {
      ...state,
      cemeteries: action.cemeteries,
    };
  }
  
  if (action.type === SET_TOMBS_BY_CEMETERY) {
    const tombsByCemetery = {
      ...(state.tombsByCemetery || {}),
    };
    
    for (let tomb of action.tombs) {
      let tombsInRow = tombsByCemetery[action.cemeteryId] &&
                        tombsByCemetery[action.cemeteryId][tomb.region] && 
                        tombsByCemetery[action.cemeteryId][tomb.region][tomb.block] && 
                        tombsByCemetery[action.cemeteryId][tomb.region][tomb.block][tomb.row] &&
                        tombsByCemetery[action.cemeteryId][tomb.region][tomb.block][tomb.row].length > 0 ? 
                        tombsByCemetery[action.cemeteryId][tomb.region][tomb.block][tomb.row] : [];
                        
      if (tombsInRow.map(tomb => tomb.id).indexOf(tomb.id) > -1) {
        tombsInRow = tombsInRow
                        .slice(0, tombsInRow.map(t => t.id).indexOf(tomb.id))
                        .concat(tomb)
                        .concat(tombsInRow.slice(tombsInRow.map(t => t.id).indexOf(tomb.id)+1));
      } else {
        tombsInRow = tombsInRow.concat(tomb);
      }
      
      tombsByCemetery[action.cemeteryId] = {
        ...(tombsByCemetery[action.cemeteryId] ? tombsByCemetery[action.cemeteryId] : {}),
        [tomb.region]: {
          ...(tombsByCemetery[action.cemeteryId] && 
              tombsByCemetery[action.cemeteryId][tomb.region] ? 
              tombsByCemetery[action.cemeteryId][tomb.region] : {}),
          [tomb.block]: {
            ...(tombsByCemetery[action.cemeteryId] && 
                tombsByCemetery[action.cemeteryId][tomb.region] && 
                tombsByCemetery[action.cemeteryId][tomb.region][tomb.block] ? 
                tombsByCemetery[action.cemeteryId][tomb.region][tomb.block] : {}),
            [tomb.row]: tombsInRow
          }
        }
      }
    }
    
    return {
      ...state,
      tombsByCemetery,
      totalTombs: action.tombs.length + state.totalTombs
    };
  }
  
  if (action.type === SET_CAPTURED) {
    return {
      ...state,
      captured: {
        ...(state.captured || {}),
        [action.tombId]: action.images
      },
    };
  }
  
  if (action.type === UPDATE_IDS_CAPTURED) {
    let captured = Object.assign({}, state.captured);
    for (let index in action.tombOldIds) {
      const tombOldId = action.tombOldIds[index];
      const tombNewId = action.tombNewIds[index];
      const oldCapturedTomb = captured[tombOldId];
      captured[tombNewId] = {
        ...oldCapturedTomb,
        _new: false,
      }
      delete captured[tombOldId];
    }
    
    return {
      ...state,
      captured,
    };
  }
  
  if (action.type === SET_CEMETERY_CAPTURED) {
    return {
      ...state,
      cemeteryCaptured: {
        ...(state.cemeteryCaptured || {}),
        [action.cemeteryId]: action.images
      },
    };
  }
  
  if (action.type === SET_NEW_TOMBS) {
    const newTombs = {};
    for (let tomb of action.tombs) {
      newTombs[tomb.id] = tomb;
    }
    
    return {
      ...state,
      newTombs: {
        ...state.newTombs,
        ...newTombs
      },
    };
  }
  
  if (action.type === SET_UPDATE_TOMBS) {
    const updateTombs = {};
    for (let tomb of action.tombs) {
      updateTombs[tomb.id] = tomb;
    }
    
    return {
      ...state,
      updateTombs: {
        ...state.updateTombs,
        ...updateTombs
      },
    };
  }
  
  if (action.type === REMOVE_NEW_TOMBS) {
    let newTombs = Object.assign({}, state.newTombs);
    for (let tombId of action.tombIds) {
      delete newTombs[tombId];
    }
    
    return {
      ...state,
      newTombs,
    };
  }
  
  if (action.type === REMOVE_UPDATE_TOMBS) {
    let updateTombs = Object.assign({}, state.updateTombs);
    for (let tombId of action.tombIds) {
      delete updateTombs[tombId];
    }

    return {
      ...state,
      updateTombs,
    };
  }
  
  if (action.type === SET_UPDATE_CEMETERY) {
    
    return {
      ...state,
      updateCemeteries: {
        ...state.updateCemeteries,
        [action.cemetery.id]: {
          ...state.updateCemeteries[action.cemetery.id],
          ...action.cemetery
        }
      },
    };
  }
  
  if (action.type === REMOVE_UPDATE_CEMETERIES) {
    let updateCemeteries = Object.assign({}, state.updateCemeteries);
    for (let cemeteryId of action.cemeteryIds) {
      delete updateCemeteries[cemeteryId];
    }

    return {
      ...state,
      updateCemeteries,
    };
  }
  
  if (action.type === RESET_TOMBS) {
    return {
      ...state,
      tombsByCemetery: {},
      lastUpdatedAt: {},
      totalTombs: 0
    };
  }
  
  if (action.type === RESET_STATE) {
    return initialState;
  }

  if (action.type === REHYDRATE) {
    const savedUser = action.payload.user || state;
    return {
      ...state,
      token: savedUser.token,
      profile: {
        ...state.profile,
        ...savedUser.profile
      },
      
      config: savedUser.config,
      bucket: savedUser.bucket,
      cdnUrl: savedUser.cdnUrl,
      
      cemeteries: savedUser.cemeteries,
      tombsByCemetery: savedUser.tombsByCemetery,
      
      lastUpdatedAt: savedUser.lastUpdatedAt,
      totalTombs: savedUser.totalTombs,
      
      captured: savedUser.captured,
      cemeteryCaptured: savedUser.cemeteryCaptured,
      newTombs: savedUser.newTombs,
      updateTombs: savedUser.updateTombs,
      updateCemeteries: savedUser.updateCemeteries,
    };
  }

  return state;
}
