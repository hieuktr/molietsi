import React, { Component } from "react";
import { connect } from 'react-redux';
import { StatusBar, View, BackHandler, Button, TouchableOpacity } from "react-native";
import { addNavigationHelpers, StackNavigator, DrawerNavigator, NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

// import DrawBar from "../components/SideBar";
import AuthenticationPage from "../components/AuthenticationPage/";
import HomePage from "../components/HomePage/";
import SyncModal from "../components/HomePage/SyncModal";
import CemeteryPage from "../components/CemeteryPage/";
import CemeteryLocationPage from "../components/CemeteryLocationPage/";
import BlockPage from "../components/BlockPage/";
import TombPage from "../components/TombPage/";

import { sync } from '../actions/user';
import { navigate } from '../actions/nav';

import { alertMe } from '../utils';

// const DrawNav = DrawerNavigator(
//   {
//     delivery_page: { screen: DeliveryPage },
//     delivered_page: { screen: DeliveredPage },
//     history_page: { screen: HistoryPage },
//   },
//   {
//     contentComponent: props => <DrawBar {...props} />
//   }
// )

export const AppNavigator = StackNavigator({
  authentication_page: {
    screen: AuthenticationPage,
    navigationOptions: {
      header: null
    }
  },
  home_page: {
    screen: HomePage,
    navigationOptions: (props) => ({
      headerStyle: {
        backgroundColor: '#FCB71E',
        elevation: 0,
      },
      headerTintColor: 'white',
      // headerRight: (
      //   <TouchableOpacity
      //     onPress={() => props.navigation.dispatch(sync())}
      //   >
      //     <Icon name="ios-sync" size={30} style={{color: 'white', paddingHorizontal: 20}}/>
      //   </TouchableOpacity>
      // ),
    })
  },
  sync_modal: {
    screen: SyncModal,
    navigationOptions: (props) => ({
      headerStyle: {
        backgroundColor: '#FCB71E',
        elevation: 0,
      },
      headerTintColor: 'white',
    })
  },
  cemetery_page: {
    screen: CemeteryPage,
    navigationOptions: (props) => ({
      headerStyle: {
        backgroundColor: '#FCB71E',
        elevation: 0,
      },
      headerTintColor: 'white',
    })
  },
  block_page: {
    screen: BlockPage,
    navigationOptions: (props) => ({
      headerStyle: {
        backgroundColor: '#FCB71E',
        elevation: 0,
      },
      headerTintColor: 'white',
    })
  },
  tomb_page: {
    screen: TombPage,
    navigationOptions: ({ navigation }) => ({
      title: "LS "+(navigation.state.params.tomb.fullname || "Chưa biết tên"),
      headerStyle: {
        backgroundColor: '#FCB71E',
        elevation: 0,
      },
      headerTintColor: 'white',
    })
  },
  cemetery_location_page: {
    screen: CemeteryLocationPage,
    navigationOptions: (props) => ({
      headerStyle: {
        backgroundColor: '#FCB71E',
        elevation: 0,
      },
      headerTintColor: 'white',
    })
  },
  // home_page: {
  //   screen: DrawNav,
  //   navigationOptions: {
  //     header: null
  //   }
  // },
});


class App extends Component {

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }
  
  onBackPress = () => {
    const { dispatch, nav, syncStatus } = this.props;
    if (nav.index > 0) {
      if (nav.index === 1 && nav.routes[1].routeName === "sync_modal" && !syncStatus.isFinish) {
        alertMe("Vui lòng chờ quá trình đồng bộ hoàn tất.");
      } else {
        dispatch(NavigationActions.back());
      }
    } else {
      alertMe("Ấn phím Trở lại 2 lần liên tiếp để thoát.");
      if (this._closedTime && new Date() - this._closedTime < 500) {
        BackHandler.exitApp();
      }
      this._closedTime = new Date();
    }
    return true;
  }

  render() {
    const { dispatch, nav } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="#FCB71E"/>
        <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  nav: state.nav,
  syncStatus: state.user.syncStatus,
});

const mapDispatchToProps = dispatch => ({
  dispatch: (action) => dispatch(action),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);