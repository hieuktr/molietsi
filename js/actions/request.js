
import { Client } from 'bugsnag-react-native';
const bugsnag = new Client();

async function parseJSON(response) {
  const data = await response.json();
  
  if (response.status !== 200) {
    console.log(response);
    
    bugsnag.notify(new Error(
      JSON.stringify(response)
    ));
  }
  
  return {
    data,
    status: response.status,
    statusText: response.statusText || response._bodyText
  };
}


export default function request(url, options) {
  return fetch(url, options)
    .then(parseJSON)
    .catch(error => {
      console.log(error);
      bugsnag.notify(error);
      let errorContent = error.toString();
      if (error.name === 'SyntaxError') {
        errorContent = "Lỗi máy chủ. Vui lòng thử lại trong giây lát.";
      } else if (error.name === 'TypeError' && error.message === 'Network request failed') {
        errorContent = "Lỗi kết nối mạng. Vui lòng kiểm tra kết nối.";
      } else {
        errorContent = "Lỗi không xác định. Vui lòng thử lại.";
      }
      return {
        status: 999,
        statusText: errorContent,
        error: {
          name: error.name,
          message: error.message
        }
      }
    });
}
