import { transferUtility } from 'react-native-s3';
import { DEFAULT_BUCKET } from '../constants';


export function init(region, access_key, secret_key) {
    return transferUtility.setupWithBasic({
      region,
      access_key,
      secret_key,
      remember_last_instance: true
    });
}

export function upload(bucket, key, file) {
    const uploadOption = {
        bucket: bucket || DEFAULT_BUCKET,
        key,
        file,
        meta: {
            "Content-Type": "image/jpg"
        }
    }
    return transferUtility.upload(uploadOption)
    // .then(result => {
    //     return new Promise((resolve, reject) => {
    //       transferUtility.subscribe(result.id, (err, task) => {
    //           console.log("task", task);
    //           if (err) {
    //               reject(err);
    //           } else {
    //               resolve(task);
    //           }
    //       });
    //     });
    // });
}

export default {init, upload};