
import { Platform } from 'react-native';

import request from './request';
import { API_URL, DEFAULT_BUCKET, DEFAULT_REGION, DEFAULT_ACCESS_KEY, DEFAULT_SECRET_KEY, DEFAULT_CDN_URL } from '../constants';

import { Client } from 'bugsnag-react-native';
const bugsnag = new Client();


export function postLogin(username, password) {
  console.log("postLogin");
  return request(`${API_URL}/login`, {
    method: 'POST',
    body: JSON.stringify({
      username, 
      password,
    }),
    headers: {
      "content-type": "application/json",
    },
  })
    .then(response => {
      if (response.status === 200) {
        console.log("Đăng nhập", response.data);
        response = {
          ...response,
          token: response.data ? response.data.token : null,
          user: response.data ? response.data.user : null
        };
      }
      
      if ((response.status === 200 && (!response.token || !response.user)) ||
          (response.status === 999 && response.statusText === "Lỗi không xác định. Vui lòng thử lại.") ||
          response.status === 403) {
        response = {
          status: 888,
          statusText: "Sai tài khoản hoặc mật khẩu. Vui lòng thử lại."
        }
      }
      
      return response;
    });
}


export function fetchVersion() {
  console.log("fetchVersion");
  return request(`${API_URL}/${Platform.OS === 'android' ? "appversion" : "iosappversion"}`)
    .then(response => {
      if (response.status === 200) {
        console.log("Kiểm tra phiên bản", response.data);
        response = {
          ...response,
          required: response.data ? !!response.data.required : false,
          version: response.data ? response.data.version : "",
          build: response.data ? response.data.build : 0,
          link: response.data ? response.data.link : "",
        };
      }

      return response;
    });
}


export function fetchConfig(token) {
  console.log("fetchConfig");
  return request(`${API_URL}/appconfig?token=${token}`)
    .then(response => {
      if (response.status === 200) {
        console.log("fetchConfig data", response.data);
        response = {
          ...response,
          config: {
            bucket: response.data && response.data.s3bucketname ? response.data.s3bucketname : DEFAULT_BUCKET,
            region: response.data && response.data.s3region ? response.data.s3region : DEFAULT_REGION,
            accessKey: response.data && response.data.access_key ? response.data.access_key : DEFAULT_ACCESS_KEY,
            secretKey: response.data && response.data.secret_key ? response.data.secret_key : DEFAULT_SECRET_KEY,
            cdnUrl: response.data && response.data.cdnbaseurl ? response.data.cdnbaseurl.replace( /\/$/g, '' ) : DEFAULT_CDN_URL,
          }
        };
      }

      return response;
    });
}


export function fetchBucket(token) {
  console.log("fetchBucket");
  return request(`${API_URL}/s3bucketname?token=${token}`)
    .then(response => {
      if (response.status === 200) {
        console.log("fetchBucket data", response.data);
        response = {
          ...response,
          bucket: response.data && response.data.bucketname ? response.data.bucketname : DEFAULT_BUCKET 
        };
      }

      return response;
    });
}


export function fetchCDNUrl(token) {
  console.log("fetchCDNUrl");
  return request(`${API_URL}/cdnbaseurl?token=${token}`)
    .then(response => {
      if (response.status === 200) {
        console.log("fetchCDNUrl data", response.data);
        response = {
          ...response,
          cdnUrl: response.data && response.data.cdnbaseurl ? 
                        response.data.cdnbaseurl.replace( /\/$/g, '' ) : DEFAULT_CDN_URL
        };
      }

      return response;
    });
}


export function fetchCemeteries(token) {
  console.log("fetchCemeteries");
  return request(`${API_URL}/cemeteries?token=${token}`)
    .then(response => {
      if (response.status === 200) {
        response = {
          ...response,
          cemeteries: response.data || []
        };
        console.log("Lấy dữ cemeteries", response.cemeteries.map(cemetery => cemetery.id), response.cemeteries);
      }

      return response;
    });
}


export function fetchTombsByCemetery(cemeteryId, token, lastUpdatedAt) {
  console.log("fetchTombsByCemetery", cemeteryId, lastUpdatedAt);
  return request(`${API_URL}/tombs/filters?token=${token}&cemetery_id=${cemeteryId}&last_updated=${lastUpdatedAt}`)
    .then(response => {
      if (response.status === 200) {
        response = {
          ...response,
          tombs: response.data && response.data.tombs ? response.data.tombs : [],
          time: response.data ? response.data.time : null,
        };
        console.log("Lấy dữ tombs của", cemeteryId, response.tombs.length, response.time);
      }

      return response;
    });
}


export function postTombs(token, tombs) {
  console.log("postTombs", tombs);
  return request(`${API_URL}/tombs/add?token=${token}`, {
    method: 'POST',
    body: JSON.stringify(tombs.map(tomb => ({
      uuid: tomb.id,
      cemetery_id: tomb.cemeteryId,
      region: tomb.region,
      block: tomb.block,
      row: tomb.row,
      no: tomb.no,
      photo_metas: tomb.photoMetas,
      wrong_martyr: tomb.wrongMartyr || false,
      wrong_props: tomb.wrongProps || false,
      no_tomb: tomb.noTomb || false,
      no_name: tomb.noName || false,
      note: tomb.note,
      full_name: tomb.fullname 
    }))),
    headers: {
      "content-type": "application/json",
    },
  })
    .then(response => {
      if (response.status === 200) {
        console.log("Tạo mới tombs", response.data);
        response = {
          ...response,
          success: response.data && response.data.success ? response.data.success : [],
          fail: response.data && response.data.fail ? response.data.fail : [],
        }
        
        if (tombs.length !== (response.fail.length + response.success.length)) {
          console.log("Lỗi dữ liệu trả về ở postTombs.");
          bugsnag.notify(new Error(
            "Lỗi dữ liệu trả về ở postTombs. " + JSON.stringify(response)
          ));
        }
      }

      return response;
    });
}


export function patchTombs(token, tombs) {
  console.log("patchTombs");
  return request(`${API_URL}/tombs/update?token=${token}`, {
    method: 'POST',
    body: JSON.stringify(tombs.map(tomb => ({
      id: tomb.id,
      wrong_martyr: tomb.wrongMartyr,
      wrong_props: tomb.wrongProps,
      no_tomb: tomb.noTomb,
      no_name: tomb.noName,
      note: tomb.note,
      photo_metas: tomb.photoMetas,
    }))),
    headers: {
      "content-type": "application/json",
    },
  })
    .then(response => {
      if (response.status === 200) {
        console.log("Cập nhật tombs", response.data);
        response = {
          ...response,
          success: response.data && response.data.success ? response.data.success : [],
          fail: response.data && response.data.fail ? response.data.fail : [],
        }
        
        if (tombs.length !== (response.fail.length + response.success.length)) {
          console.log("Lỗi dữ liệu trả về ở patchTombs.");
          bugsnag.notify(new Error(
            "Lỗi dữ liệu trả về ở patchTombs. " + JSON.stringify(response) + JSON.stringify(tombs)
          ));
        }
      }

      return response;
    });
}


export function patchCemeteries(token, cemeteries) {
  console.log("patchTombs");
  return request(`${API_URL}/cemeteries/gps?token=${token}`, {
    method: 'POST',
    body: JSON.stringify(cemeteries),
    headers: {
      "content-type": "application/json",
    },
  })
    .then(response => {
      if (response.status === 200) {
        console.log("Cập nhật cemeteries", response.data);
        response = {
          ...response,
          cemeteries: response.data || [],
        }
      }

      return response;
    });
}
