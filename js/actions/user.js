
import { NetInfo, Alert, Platform } from 'react-native';
import { transferUtility } from 'react-native-s3';
import RNFS from 'react-native-fs';
import RNExitApp from 'react-native-exit-app';

import { resetRoute } from './nav';
import s3 from './s3';
import { postLogin, fetchVersion, fetchBucket, fetchCDNUrl, fetchConfig,
  fetchCemeteries, fetchTombsByCemetery, postTombs, patchTombs, patchCemeteries } from './services';
import { DEFAULT_BUCKET, DEFAULT_REGION, DEFAULT_ACCESS_KEY, DEFAULT_SECRET_KEY } from '../constants';

import { Client } from 'bugsnag-react-native';
const bugsnag = new Client();


export const SET_PROFILE = 'SET_PROFILE';
export const SET_TOKEN = 'SET_TOKEN';
export const SET_AUTH_STATUS = 'SET_ACTIVE_STATUS';
export const SET_UPDATE_STATUS = 'SET_UPDATE_STATUS';
export const SET_LAST_UPDATED_AT = 'SET_LAST_UPDATED_AT';

export const SET_CONFIG = 'SET_CONFIG';
export const SET_BUCKET = 'SET_BUCKET';
export const SET_CDN_URL = 'SET_CDN_URL';

export const SET_SYNC_STATUS = 'SET_SYNC_STATUS';
export const ADD_SYNC_STATUS_CONTENT = 'ADD_SYNC_STATUS_CONTENT';

export const SET_CEMETERIES = 'SET_CEMETERIES';
export const SET_TOMBS_BY_CEMETERY = 'SET_TOMBS_BY_CEMETERY';

export const SET_CAPTURED = 'SET_CAPTURED';
export const UPDATE_IDS_CAPTURED = 'UPDATE_IDS_CAPTURED';

export const SET_CEMETERY_CAPTURED = 'SET_CEMETERY_CAPTURED';

export const SET_NEW_TOMBS = 'SET_NEW_TOMBS';
export const SET_UPDATE_TOMBS = 'SET_UPDATE_TOMBS';
export const SET_UPDATE_CEMETERY = 'SET_UPDATE_CEMETERY';

export const REMOVE_NEW_TOMBS = 'REMOVE_NEW_TOMBS';
export const REMOVE_UPDATE_TOMBS = 'REMOVE_UPDATE_TOMBS';
export const REMOVE_UPDATE_CEMETERIES = 'REMOVE_UPDATE_CEMETERIES';

export const RESET_TOMBS = 'RESET_TOMBS';
export const RESET_STATE = 'RESET_STATE';

function normalizeCemetery(cemetery) {
  return {
    id: cemetery.id,
    name: cemetery.name,
    total: cemetery.Total || 0,
    finished: cemetery.Finished || 0,
    photos: cemetery.photos ? cemetery.photos : [],
    locations: cemetery.gpses ? cemetery.gpses : [],
  }
}

function normalizeTomb(tomb) {
  return {
    id: tomb.id,
    cemeteryId: tomb.cemetery_id,
    row: tomb.row,
    col: tomb.col,
    no: tomb.no,
    block: tomb.block,
    region: tomb.region,
    area: tomb.area,
    fullname: tomb.full_name,
    dob: tomb.dob,
    mob: tomb.mob,
    yob: tomb.yob,
    dod: tomb.dod,
    mod: tomb.mod,
    yod: tomb.yod,
    town: tomb.town,
    district: tomb.district,
    province: tomb.province,
    rank: tomb.rank,
    position: tomb.position,
    unit: tomb.unit,
    modifiedByUserId: tomb.modified_by_user_id,
    managedBy: tomb.managed_by,
    wrongMartyr: tomb.wrong_martyr,
    wrongProps: tomb.wrong_props,
    noTomb: tomb.no_tomb,
    noName: tomb.no_name,
    note: tomb.note,
    photoMetas: tomb.photo_metas,
    photoMain: tomb.photo_bia,
    photoPortrait: tomb.photo_portrait,
    photoLandscape: tomb.photo_landscape,
  }
}

export function setProfile(profile) {
  return {
    type: SET_PROFILE,
    profile,
  };
}

export function setToken(token) {
  return {
    type: SET_TOKEN,
    token,
  };
}

export function setConfig(config) {
  return {
    type: SET_CONFIG,
    config,
  };
}

export function setBucket(bucket) {
  return {
    type: SET_BUCKET,
    bucket,
  };
}

export function setCDNUrl(cdnUrl) {
  return {
    type: SET_CDN_URL,
    cdnUrl,
  };
}

export function setAuthStatus(status, error='') {
  return {
    type: SET_AUTH_STATUS,
    status,
    error,
  };
}

export function setUpdateStatus(status) {
  return {
    type: SET_UPDATE_STATUS,
    status,
  };
}

export function setSyncStatus(syncStatus) {
  return {
    type: SET_SYNC_STATUS,
    syncStatus,
  };
}

export function addSyncStatusContent(content, isFinish=false, total=0) {
  return {
    type: ADD_SYNC_STATUS_CONTENT,
    content,
    isFinish,
    total
  };
}

export function setSyncStatusUploadIncrease(status) {
  return (dispatch, getState) => {
    const oldFail = getState().user.syncStatus.fail;
    const fail = oldFail + (status === "fail" ? 1 : 0);
    const oldSuccess = getState().user.syncStatus.success;
    const success = oldSuccess + (status === "success" ? 1 : 0);
    const total = getState().user.syncStatus.total;
    let content = getState().user.syncStatus.content+"";
    
    if (status === "success") {
      if (content.indexOf(`Tải lên thành công ${oldSuccess} ảnh`) > -1) {
        content = content.replace(`Tải lên thành công ${oldSuccess} ảnh`, `Tải lên thành công ${success} ảnh`);
      } else {
        content = content + '\n' + `Tải lên thành công ${success} ảnh`
      }
    }
    
    if (status === "fail") {
      if (content.indexOf(`Tải lên thất bại ${oldFail} ảnh`) > -1) {
        content = content.replace(`Tải lên thất bại ${oldFail} ảnh`, `Tải lên thất bại ${fail} ảnh`);
      } else {
        content = content + '\n' + `Tải lên thất bại ${fail} ảnh`
      }
    }
    
    dispatch(setSyncStatus({
      ...getState().user.syncStatus,
      content,
      fail,
      success,
    }));
    
    // if (fail + success === total) {
    //   dispatch(addSyncStatusContent(`Đồng bộ kết thúc.`, true));
    // }
  }
}

export function setCemeteries(cemeteries) {
  return {
    type: SET_CEMETERIES,
    cemeteries,
  };
}

export function setTombs(tombs) {
  return {
    type: SET_TOMBS,
    tombs,
  };
}

export function setCaptured(tombId, images) {
  return {
    type: SET_CAPTURED,
    tombId,
    images,
  };
}

export function updateIdsCaptured(tombOldIds, tombNewIds) {
  return {
    type: UPDATE_IDS_CAPTURED,
    tombOldIds,
    tombNewIds,
  };
}

export function setCemeteryCaptured(cemeteryId, images) {
  return {
    type: SET_CEMETERY_CAPTURED,
    cemeteryId,
    images,
  };
}

export function setCapturedUploaded(tombId, imageType) {
  return (dispatch, getState) => {
    const capturedImagesByTomb = {
      ...getState().user.captured[tombId],
    }
    delete capturedImagesByTomb[imageType];
    
    dispatch(setCaptured(
      tombId,
      capturedImagesByTomb
    ));
  }
}

export function setCemeteryCapturedUploaded(cemeteryId, index) {
  return (dispatch, getState) => {
    const cemeteryCaptured = getState().user.cemeteryCaptured;
    const oldCapturedCemetery = cemeteryCaptured && cemeteryCaptured[cemeteryId] && typeof cemeteryCaptured[cemeteryId] === 'object' ? cemeteryCaptured[cemeteryId] : {};
    delete oldCapturedCemetery[index];
    
    dispatch(setCemeteryCaptured(
      cemeteryId, 
      oldCapturedCemetery
    ));
  }
}

export function setNewTombs(tombs) {
  console.log("setNewTombs", tombs);
  return {
    type: SET_NEW_TOMBS,
    tombs,
  };
}

export function removeNewTombs(tombIds) {
  return {
    type: REMOVE_NEW_TOMBS,
    tombIds,
  };
}

export function setUpdateTombs(tombs) {
  console.log("setUpdateTombs", tombs);
  return {
    type: SET_UPDATE_TOMBS,
    tombs,
  };
}

export function removeUpdateTombs(tombIds) {
  return {
    type: REMOVE_UPDATE_TOMBS,
    tombIds,
  };
}

export function setUpdateCemetery(cemetery) {
  console.log("setUpdateCemetery", cemetery);
  return {
    type: SET_UPDATE_CEMETERY,
    cemetery,
  };
}

export function removeUpdateCemeteries(cemeteryIds) {
  return {
    type: REMOVE_UPDATE_CEMETERIES,
    cemeteryIds,
  };
}

export function checkUpdate() {
  return async (dispatch, getState) => {
    
    const fetchVersionResponse = await fetchVersion();
    
    if (fetchVersionResponse.status === 200) {
      dispatch(setUpdateStatus({
        required: fetchVersionResponse.required,
        link: fetchVersionResponse.link,
        version: fetchVersionResponse.version,
        build: fetchVersionResponse.build,
      }));
    }
  };
}

export function getConfig() {
  return async (dispatch, getState) => {
    const { token } = getState().user;
    const response = await fetchConfig(token);
    
    if (response.status === 200) {
      dispatch(setConfig(response.config));
    }
  };
}

export function getBucket() {
  return async (dispatch, getState) => {
    const { token } = getState().user;
    const response = await fetchBucket(token);
    
    if (response.status === 200) {
      dispatch(setBucket(response.bucket));
    }
  };
}

export function getCDNUrl() {
  return async (dispatch, getState) => {
    const { token } = getState().user;
    const response = await fetchCDNUrl(token);
    
    if (response.status === 200) {
      dispatch(setCDNUrl(response.cdnUrl));
    }
  };
}

export function login(username, password) {
  return async (dispatch, getState) => {
    dispatch(setAuthStatus('loading'));
    const postLoginResponse = await postLogin(username, password);
    
    if (postLoginResponse.status === 200) {
      if (postLoginResponse.user) {
        dispatch(setProfile({
          id: postLoginResponse.user.id,
          role: postLoginResponse.user.role,
          unit: postLoginResponse.user.unit,
          email: postLoginResponse.user.email,
          phone: postLoginResponse.user.phone,
          displayName: postLoginResponse.user.display_name,
          username: postLoginResponse.user.username,
        }));
      }
      
      dispatch(setToken(postLoginResponse.token));
      dispatch(setAuthStatus('completed'));
      dispatch(resetRoute('home_page'));
      // dispatch(getData());
      dispatch(getConfig());
    } else {
      dispatch(setAuthStatus('failed', postLoginResponse.statusText));
    }
  };
}

export function logout() {
  return (dispatch, getState) => {
    dispatch({type: RESET_STATE});
    setTimeout(() => RNExitApp.exitApp(), 500);
  }
}

export function getData() {
  return async (dispatch, getState) => {
    dispatch(setAuthStatus('loading'));
    
    const { token } = getState().user;
    const cemeteriesResponse = await fetchCemeteries(token);
    
    if (cemeteriesResponse.status === 200) {
      dispatch(setCemeteries(cemeteriesResponse.cemeteries.map(cemetery => normalizeCemetery(cemetery))));
      
      for (const cemetery of cemeteriesResponse.cemeteries) {
        const { lastUpdatedAt, totalTombs } = getState().user;
        const tombsResponse = await fetchTombsByCemetery(cemetery.id, token, lastUpdatedAt ? lastUpdatedAt[cemetery.id] : null);
        
        if (tombsResponse.status === 200) {
          if (tombsResponse.tombs.length + totalTombs > 2000) {
            console.log("Sắp quá dung lượng lưu trữ, không thể lưu thêm.", totalTombs, tombsResponse.tombs.length, tombsResponse.tombs.length + totalTombs);
            dispatch({ type: RESET_TOMBS });
            break;
          }
          
          if (tombsResponse.tombs.length > 0) {
            dispatch({
              type: SET_TOMBS_BY_CEMETERY,
              cemeteryId: cemetery.id,
              tombs: tombsResponse.tombs.map(tomb => normalizeTomb(tomb))
            });
            dispatch({
              type: SET_LAST_UPDATED_AT,
              cemeteryId: cemetery.id,
              time: tombsResponse.time
            });
          }
        }
      }
    } else if (cemeteriesResponse.status === 500 || cemeteriesResponse.status === 401) {
      dispatch(setToken(''));
      dispatch(resetRoute('authentication_page'));
    } 
    
    dispatch(setAuthStatus('completed'));
  };
}


export function createTombs() {
  return async (dispatch, getState) => {
    dispatch(addSyncStatusContent(`Kiểm tra mộ cần tạo mới`));
    
    const token = getState().user.token;
    const newTombs = Object.keys(getState().user.newTombs)
                          .map(tombId => getState().user.newTombs[tombId]);
                          
    if (newTombs.length > 0) {
      dispatch(addSyncStatusContent(`Đang tạo mới ${newTombs.length} mộ`));
      
      const postTombsResponse = await postTombs(token, newTombs);
      
      if (postTombsResponse.status === 200) {
        const oldNewTombIds = newTombs
                                  .filter(tomb => postTombsResponse.fail.map(tomb => tomb.uuid).indexOf(tomb.id) < 0)
                                  .map(tomb => tomb.id);
        const newNewTombIds = postTombsResponse.success.map(tomb => tomb.id);
        dispatch(updateIdsCaptured(oldNewTombIds, newNewTombIds));
        dispatch(removeNewTombs(oldNewTombIds));
        // dispatch(getData());
        
        dispatch(addSyncStatusContent(`Đã tạo mới mộ. Thành công ${postTombsResponse.success.length}. Thất bại ${postTombsResponse.fail.length}`));
      } else if (postTombsResponse.status === 500 || postTombsResponse.status === 401) {
        dispatch(setToken(''));
        dispatch(resetRoute('authentication_page'));
        return 'end';
      } else {
        dispatch(addSyncStatusContent(`Tạo mới mộ lỗi: ${postTombsResponse.statusText}`));
      }
    }
  };
}

export function editTombs() {
  return async (dispatch, getState) => {
    dispatch(addSyncStatusContent(`Kiểm tra mộ cần cập nhật`));
    
    const token = getState().user.token;
    const updateTombs = Object.keys(getState().user.updateTombs)
                          .map(tombId => getState().user.updateTombs[tombId]);

    if (updateTombs.length > 0) {
      dispatch(addSyncStatusContent(`Đang cập nhật dữ liệu ${updateTombs.length} mộ`));
      
      const patchTombsResponse = await patchTombs(token, updateTombs);

      if (patchTombsResponse.status === 200) {
        dispatch(removeUpdateTombs(patchTombsResponse.success.map(tomb => tomb.id)));
        // dispatch(getData());
        
        dispatch(addSyncStatusContent(`Đã cập nhật dữ liệu mộ. Thành công ${patchTombsResponse.success.length}. Thất bại ${patchTombsResponse.fail.length}`));
      } else if (patchTombsResponse.status === 500 || patchTombsResponse.status === 401) {
        dispatch(setToken(''));
        dispatch(resetRoute('authentication_page'));
        return 'end';
      } else {
        dispatch(addSyncStatusContent(`Đang cập nhật dữ liệu mộ lỗi: ${patchTombsResponse.statusText}`));
      }
    }
  };
}


export function editCemeteries() {
  return async (dispatch, getState) => {
    dispatch(addSyncStatusContent(`Kiểm tra nghĩa trang cần cập nhật`));
    
    const token = getState().user.token;
    const updateCemeteries = Object.keys(getState().user.updateCemeteries)
                              .map(cemeteryId => {
                                const cemetery = getState().user.updateCemeteries[cemeteryId]
                                return {
                                  id: cemetery.id,
                                  gpses: Object.keys(cemetery.locations).map(locationName => ({
                                    position: locationName,
                                    ...cemetery.locations[locationName],
                                  }))
                                }
                              });

    if (updateCemeteries.length > 0) {
      dispatch(addSyncStatusContent(`Đang cập nhật dữ liệu ${updateCemeteries.length} nghĩa trang`));
      
      const patchCemeteriesResponse = await patchCemeteries(token, updateCemeteries);

      if (patchCemeteriesResponse.status === 200) {
        dispatch(removeUpdateCemeteries(patchCemeteriesResponse.cemeteries.map(cemeteryId => cemeteryId)));
        // dispatch(getData());

        dispatch(addSyncStatusContent(`Đã cập nhật dữ liệu nghĩa trang. Thành công ${patchCemeteriesResponse.cemeteries.length}. Thất bại ${updateCemeteries.length - patchCemeteriesResponse.cemeteries.length}`));
      } else if (patchCemeteriesResponse.status === 500 || patchCemeteriesResponse.status === 401) {
        dispatch(setToken(''));
        dispatch(resetRoute('authentication_page'));
        return 'end';
      } else {
        dispatch(addSyncStatusContent(`Đang cập nhật dữ liệu nghĩa trang lỗi: ${patchCemeteriesResponse.statusText}`));
      }
    }
  };
}


export function sync() {
  return async (dispatch, getState) => {
    dispatch(setSyncStatus({
      isFinish: false,
      content: `Bắt đầu đồng bộ dữ liệu`,
    }));
    
    const editTombsResponse = await dispatch(editTombs());
    if (editTombsResponse !== 'end') {
      const createTombsResponse = await dispatch(createTombs());
      if (createTombsResponse !== 'end') {
        const editCemeteriesResponse = await dispatch(editCemeteries());
        if (editCemeteriesResponse !== 'end') {
          NetInfo.getConnectionInfo().then((connectionInfo) => {
            console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
            dispatch(uploadImages(connectionInfo.type));
          });
        } else {
          dispatch(addSyncStatusContent(`Đồng bộ kết thúc.`, true));
        }
      } else {
        dispatch(addSyncStatusContent(`Đồng bộ kết thúc.`, true));
      }
    } else {
      dispatch(addSyncStatusContent(`Đồng bộ kết thúc.`, true));
    }
  }
}


export function uploadImages(networkType) {
  return async (dispatch, getState) => {
    dispatch(addSyncStatusContent(`Kiểm tra ảnh cần đồng bộ`));
    const { token, captured, cemeteryCaptured, config } = getState().user;
    
    let needUploadImages = [];
    for (let tombId in captured) {
      const tomb = captured[tombId];
      if (!tomb._new) {
        if (tomb.main && tomb.main.uri && !tomb.main.isUploaded) {
          needUploadImages.push({
            key: !tomb.photoMain ? `${tombId}/Bia.jpg` : `${tombId}/1${tomb.photoMain.split("/").slice(-1)[0]}`,
            file: Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}/molietsi/${tomb.main.uri}` : tomb.main.uri,
            tombId,
            type: "main"
          })
        }
        if (tomb.portrait && tomb.portrait.uri && !tomb.portrait.isUploaded) {
          needUploadImages.push({
            key: !tomb.photoPortrait ? `${tombId}/Portrait.jpg` : `${tombId}/1${tomb.photoPortrait.split("/").slice(-1)[0]}`,
            file: Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}/molietsi/${tomb.portrait.uri}` : tomb.portrait.uri,
            tombId,
            type: "portrait"
          })
        }
        if (tomb.landscape && tomb.landscape.uri && !tomb.landscape.isUploaded) {
          needUploadImages.push({
            key: !tomb.photoLandscape ? `${tombId}/Landscape.jpg` : `${tombId}/1${tomb.photoLandscape.split("/").slice(-1)[0]}`,
            file: Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}/molietsi/${tomb.landscape.uri}` : tomb.landscape.uri,
            tombId,
            type: "landscape"
          })
        }
      }
    }
    
    try {
      for (let cemeteryId in cemeteryCaptured) {
        const cemeteryImages = cemeteryCaptured && cemeteryCaptured[cemeteryId] && typeof cemeteryCaptured[cemeteryId] === 'object' ? cemeteryCaptured[cemeteryId] : {};
        needUploadImages = needUploadImages.concat(
          Object.keys(cemeteryImages)
            .map(k => ({
              key: `${cemeteryId}/${k}.jpg`,
              file: Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}/molietsi/${cemeteryImages[k].uri}` : cemeteryImages[k].uri,
              cemeteryId,
              index: k,
            }))
        );
      }
    } catch(e) {
      bugsnag.notify(new Error(
        `${e.toString()} ${JSON.stringify(cemeteryCaptured)}`
      ));
    }
    
    console.log(needUploadImages);
    
    if (needUploadImages.length > 0) {
      dispatch(addSyncStatusContent(`Đang đồng bộ ${needUploadImages.length} ảnh`));
      
      const _upload = async () => {
        dispatch(addSyncStatusContent(`Bắt đầu đồng bộ dữ liệu ${needUploadImages.length} ảnh`, false, needUploadImages.length));
        
        let { bucket, region, accessKey, secretKey } = config || {
          bucket: DEFAULT_BUCKET,
          region: DEFAULT_REGION,
          accessKey: DEFAULT_ACCESS_KEY,
          secretKey: DEFAULT_SECRET_KEY
        };
        const configResponse = await fetchConfig(token);
        if (configResponse.status === 200) {
          bucket = configResponse.config.bucket;
          region = configResponse.config.region;
          accessKey = configResponse.config.accessKey;
          secretKey = configResponse.config.secretKey;
          dispatch(setConfig(configResponse.config));
        }
        
        await s3.init(region, accessKey, secretKey).then(async ret => {
          console.log(`Bắt đầu tải ảnh`);
          if (ret) {
            for (let image of needUploadImages) {
              console.log(`check exist ${Platform.OS === 'android' ? image.file.replace("file://", "") : image.file}`);
              if (await RNFS.exists(Platform.OS === 'android' ? image.file.replace("file://", "") : image.file)) {
                console.log(`uploading ${image.file} to ${image.key}`);
                const uploadResponse = await s3.upload(bucket, image.key, image.file)
                                                .catch(e => {
                                                  console.log(e);
                                                });
                console.log(`uploadResponse: ${JSON.stringify(uploadResponse)}`);
                
                if (uploadResponse && uploadResponse.id) {
                  const uploadedStatus = await new Promise((resolve, reject) => {
                    transferUtility.subscribe(uploadResponse.id, (err, task) => {
                      console.log("subscribe", err, task);
                      if (err) {
                        transferUtility.unsubscribe(uploadResponse.id);
                        reject("error");
                      } else if (task && (task.state === "completed" || task.state === "failed" || task.state === "canceled")) {
                        transferUtility.unsubscribe(uploadResponse.id);
                        resolve(task.state);
                      }
                    });
                  });
                  
                  console.log("uploadedStatus", uploadedStatus);
                  
                  if (uploadedStatus === "completed") {
                    if (image.tombId) {
                      dispatch(setCapturedUploaded(image.tombId, image.type));
                    } else {
                      dispatch(setCemeteryCapturedUploaded(image.cemeteryId, image.index));
                    }
                    dispatch(setSyncStatusUploadIncrease("success"));
                    
                    await RNFS.unlink(Platform.OS === 'android' ? image.file.replace("file://", "") : image.file)
                        .then(() => {
                          console.log('FILE DELETED');
                        })
                        .catch((err) => {
                          console.log('FILE DELETE FAIL', err.message);
                        });
                  } else {
                    dispatch(setSyncStatusUploadIncrease("fail"));
                  }
                  
                }
              } else {
                console.log("File doesn't exist", image.file);
                
                if (image.tombId) {
                  dispatch(setCapturedUploaded(image.tombId, image.type));
                } else {
                  dispatch(setCemeteryCapturedUploaded(image.cemeteryId, image.index));
                }
                dispatch(setSyncStatusUploadIncrease("fail"));
                
                bugsnag.notify(new Error(
                  `File doesn't exist ${image.file}. UserId: ${getState().user.profile.id}`
                ));
              }
            }
            
            dispatch(addSyncStatusContent(`Đồng bộ kết thúc.`, true));
          }
          else {
            dispatch(addSyncStatusContent(`Khởi tạo S3 lỗi`));
            dispatch(addSyncStatusContent(`Đồng bộ kết thúc.`, true));
          }
        });
      }
      
      if (networkType === 'cellular') {
        Alert.alert(
          'Sử dụng 3G/4G để tải ảnh lên',
          'Bạn đang không kết nối Wifi. Thao tác này sẽ tiêu tốn nhiều dung lượng 3G/4G. Bạn có chắc chắn?',
          [
            {text: 'Huỷ', onPress: () => {
              console.log('Cancel Pressed');
              dispatch(addSyncStatusContent(`Đồng bộ kết thúc.`, true));
            }, style: 'cancel'},
            {text: 'Đồng ý', onPress: () => {_upload()}},
          ]
        );
      } else {
        _upload();
      }
    } else {
      dispatch(addSyncStatusContent(`Không có ảnh cần đồng bộ`));
      dispatch(addSyncStatusContent(`Đồng bộ kết thúc.`, true));
    }
  };
}
