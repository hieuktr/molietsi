import { Platform } from 'react-native';
import RNFS from 'react-native-fs';
import { transferUtility } from 'react-native-s3';

import s3 from '../actions/s3';

import { Client } from 'bugsnag-react-native';
const bugsnag = new Client();

const logPath = RNFS.DocumentDirectoryPath + '/log.txt';

export function writeLog(content) {
  // RNFS.write(logPath, `${new Date().toISOString()}: ${content}\n\r`, -1, 'utf8')
  //   .then((success) => {
  //     console.log('LOG WRITTEN!');
  //   })
  //   .catch((err) => {
  //     console.log('WRITE LOG ERROR', err.message);
  //     bugsnag.notify(err);
  //   });
}

export async function uploadLogThenRemove(key) {
  // s3.init().then(async ret => {
  //   if (ret) {
  //     if (await RNFS.exists(logPath)) {
  //       const uploadResponse = await s3.upload(key, Platform.OS === 'android' ? "file://" + logPath : logPath)
  //                                       .catch(err => {
  //                                         console.log("REQUEST UPLOAD LOG ERROR", err);
  //                                         writeLog("REQUEST UPLOAD LOG ERROR " + err.toString());
  //                                         bugsnag.notify(err);
  //                                       });
  // 
  //       if (uploadResponse && uploadResponse.id) {
  //         transferUtility.subscribe(uploadResponse.id, (err, task) => {
  //           if (err) {
  //             transferUtility.unsubscribe(uploadResponse.id);
  //             console.log("UPLOAD LOG ERROR", err);
  //             writeLog("UPLOAD LOG ERROR " + err.toString());
  //             bugsnag.notify(new Error(JSON.stringify(err)));
  //             RNFS.unlink(logPath)
  //               .then(() => {
  //                 console.log('LOG DELETED');
  //               })
  //               .catch((err) => {
  //                 console.log('DELETE LOG ERROR', err.message);
  //                 writeLog("DELETE LOG ERROR" + err.toString());
  //                 bugsnag.notify(err);
  //               });
  //           } else {
  //             if (task.state == "completed") {
  //               console.log("UPLOAD LOG COMPLETED", task);
  //               RNFS.unlink(logPath)
  //                 .then(() => {
  //                   console.log('LOG DELETED');
  //                 })
  //                 .catch((err) => {
  //                   console.log('DELETE LOG ERROR', err.message);
  //                   writeLog("DELETE LOG ERROR" + err.toString());
  //                   bugsnag.notify(err);
  //                 });
  // 
  //               transferUtility.unsubscribe(uploadResponse.id);
  //             } else if (task.state == "failed") {
  //               transferUtility.unsubscribe(uploadResponse.id);
  //               console.log("UPLOAD LOG FAILED");
  //               writeLog("UPLOAD LOG FAILED");
  //               bugsnag.notify(new Error(
  //                 "UPLOAD LOG FAILED " + logPath
  //               ));
  //               RNFS.unlink(logPath)
  //                 .then(() => {
  //                   console.log('LOG DELETED');
  //                 })
  //                 .catch((err) => {
  //                   console.log('DELETE LOG ERROR', err.message);
  //                   writeLog("DELETE LOG ERROR" + err.toString());
  //                   bugsnag.notify(err);
  //                 });
  //             }
  //           }
  //         });
  //       }
  //     } else {
  //       bugsnag.notify(new Error(
  //         "LOG DOESN'T EXIST " + logPath
  //       ));
  //       console.log("LOG DOESN'T EXIST", logPath);
  //     }
  //   } else {
  //     writeLog(`INIT S3 ERROR`);
  //     console.log(`INIT S3 ERROR`);
  //   }
  // });
}
