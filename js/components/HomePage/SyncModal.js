
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, Text, Alert, Platform, Dimensions, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import DeviceInfo from 'react-native-device-info';

import { goBack } from '../../actions/nav';
import { sync, getData } from '../../actions/user';

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
}

class SyncModal extends Component {
  
  static navigationOptions = ({ navigation }) => ({
    title: "ĐỒNG BỘ DỮ LIỆU",
    headerLeft: null,
  });
  
  componentWillMount() {
    this.props.sync();
  }
  
  componentWillUnmount() {
    this.props.getData();
  }

  render() {
    const { goBack, sync, syncStatus } = this.props;
    
    return (
      <View style={styles.container}>
        <ScrollView style={{
          flex: 1, 
          padding: 20,
        }}>
          <Text>{syncStatus.content}</Text>
        </ScrollView>
        <Text style={{ marginHorizontal: 10, marginBottom: 10, fontSize: 13 }}>
          {`Phiên bản hiện tại ${DeviceInfo.getVersion()} (${DeviceInfo.getBuildNumber()})`}
        </Text>
        {syncStatus.isFinish ? (
          <View style={{flexDirection: 'row', padding: 8}}>
            <TouchableOpacity onPress={() => sync()} style={{
              flex: 1,
              alignItems: 'center',
              backgroundColor: '#e2e2e2',
              padding: 10,
              borderRadius: 5,
              backgroundColor: '#55AADD'
            }}>
              <Text style={{fontSize: 18}}>Đồng bộ lại</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => goBack()} style={{
              flex: 1,
              alignItems: 'center',
              backgroundColor: '#e2e2e2',
              padding: 10,
              borderRadius: 5,
              marginLeft: 8,
              backgroundColor: '#6AC34F'
            }}>
              <Text style={{fontSize: 18}}>Hoàn tất</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <Text style={{
            backgroundColor: '#e2e2e2',
            padding: 10,
            textAlign: 'center',
            fontSize: 18,
            fontWeight: 'bold',
            borderRadius: 5,
            margin: 8
          }}>Đang đồng bộ...</Text>
        )}
      </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    goBack: () => dispatch(goBack()),
    sync: () => dispatch(sync()),
    getData: () => dispatch(getData()),
  };
}

function mapStateToProps(state) {
  return {
    syncStatus: state.user.syncStatus,
  };
}

export default connect(mapStateToProps, bindAction)(SyncModal);
