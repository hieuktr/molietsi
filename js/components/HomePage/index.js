
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, FlatList, TouchableOpacity, Text, Alert, Platform, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { navigate } from '../../actions/nav';
import { getData, logout, getConfig } from '../../actions/user';
// import { PERIOD_TIME_GET_UPDATE } from '../../constants';

const styles = {
  footerTab: {
    backgroundColor: '#FCB71E',
    flexDirection: 'row',
    padding: 10
  },
  modal: {
    width: Dimensions.get('window').width - 40,
    marginTop: 100,
    padding: 30
  },
}

class HomePage extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
    }
  }
  
  static navigationOptions = ({ navigation }) => ({
    title: "DANH SÁCH NGHĨA TRANG",
    // headerRight: (
    //   <TouchableOpacity
    //     onPress={() => navigation.state.params.openInfoModal()}
    //   >
    //     <Icon name="ios-add" size={30} style={{color: 'white', paddingHorizontal: 20}}/>
    //   </TouchableOpacity>
    // ),
  });
  
  componentWillMount() {
    // if (Platform.OS === 'android') {
      // this._interval = setInterval(() => this.props.getData(), PERIOD_TIME_GET_UPDATE);
    // }
    // console.log("mount");
    this.props.getData();
    // this.props.getConfig();
  }
  
  componentDidMount() {
    // this.props.navigation.setParams({
    //   openInfoModal: this.refs.infoModal.open,
    // });
  }
  
  componentWillUnmount() {
    // try {
    //   clearInterval(this._interval);
    // } catch(e) {console.log(e)}
  }
  
  onItemPress(cemetery) {
    this.props.navigate({ routeName: "cemetery_page", params: {cemetery} });
  }
  
  onExit() {
    Alert.alert(
      'Đăng xuất ứng dụng',
      'Bạn có chắc chắn?',
      [
        {text: 'Huỷ', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Thoát', onPress: () => this.props.logout()},
      ]
    );
  }
  
  logoutIsDisabled() {
    const { captured, cemeteryCaptured, updateTombs, newTombs, updateCemeteries } = this.props;
    if (captured) {
      for (let tombId in captured) {
        if (captured[tombId].main && !captured[tombId].main.isUploaded) {
          return true;
        }
        if (captured[tombId].portrait && !captured[tombId].portrait.isUploaded) {
          return true;
        }
        if (captured[tombId].landscape && !captured[tombId].landscape.isUploaded) {
          return true;
        }
      }
    }
    if (cemeteryCaptured) {
      for (let cemeteryId in cemeteryCaptured) {
        if (cemeteryCaptured[cemeteryId] && typeof cemeteryCaptured[cemeteryId] === "object" && Object.keys(cemeteryCaptured[cemeteryId]).length > 0) {
          return true;
        }
      }
    }
    if (newTombs && Object.keys(newTombs).length > 0) {
      return true;
    }
    if (updateTombs && Object.keys(updateTombs).length > 0) {
      return true;
    }
    if (updateCemeteries && Object.keys(updateCemeteries).length > 0) {
      return true;
    }
  }

  render() {
    const { cemeteries, getData, authStatus, syncStatus } = this.props;
    
    return (
      <View style={{flex: 1}}>
        {cemeteries.length > 0 ? (
          <FlatList 
            style={{margin: 10, flex: 1}}
            refreshing={authStatus === 'loading'}
            onRefresh={getData}
            data={cemeteries}
            keyExtractor={cemetery => cemetery.id+""}
            renderItem={({item: cemetery, index}) => {
              return (
                <TouchableOpacity 
                  onPress={() => this.onItemPress(cemetery)}
                  style={{
                    backgroundColor: 'white',
                    padding: 20,
                    marginBottom: 10,
                  }}
                  activeOpacity={0.7}>
                  <View>
                    <Text style={{ fontSize: 18, marginBottom: 8 }}>{cemetery.name}</Text>
                    <Text style={{ fontSize: 14 }}>{`Hoàn thành ${cemetery.finished}/${cemetery.total}`}</Text>
                  </View>
                </TouchableOpacity>
              );
            }}
          />
        ) : (
          <FlatList 
            style={{margin: 10, flex: 1}}
            refreshing={authStatus === 'loading'}
            onRefresh={getData}
            data={Array(1).fill()}
            keyExtractor={(cemetery, index) => index+""}
            renderItem={({item: cemetery, index}) => {
              return (
                <Text style={{fontSize: 17, lineHeight: 30, alignSelf: 'center', marginTop: 20, flex: 1}}>Không có nghĩa trang</Text>
              );
            }}
          />
        )}
        <View style={styles.footerTab}>
          <TouchableOpacity
            style={{flex: 1, alignItems: 'center'}}
            // onPress={() => sync()}
            onPress={() => this.props.navigate({routeName: 'sync_modal'})}
            disabled={!syncStatus.isFinish}
          >
            <Text style={{color: !syncStatus.isFinish ? '#fbcb63' : 'white', fontSize: 16}}>
              {!syncStatus.isFinish ? "Đang đồng bộ" : "Đồng bộ"}
            </Text>
            <Icon name="ios-sync" size={30} style={{color: !syncStatus.isFinish ? '#fbcb63' : 'white'}}/>
          </TouchableOpacity>
          <TouchableOpacity
            style={{flex: 1, alignItems: 'center'}}
            onPress={() => this.onExit()}
            disabled={this.logoutIsDisabled()}
          >
            <Text style={{color: this.logoutIsDisabled() ? '#fbcb63' : 'white', fontSize: 16}}>
              Đăng xuất
            </Text>
            <Icon name="ios-exit-outline" size={30} style={{color: this.logoutIsDisabled() ? '#fbcb63' : 'white'}}/>
          </TouchableOpacity>
        </View>
        
        {/*<Modal 
          style={{...styles.modal, height: 300}} 
          backButtonClose={true} 
          position={"top"} 
          ref={"infoModal"}>
          <Text style={{fontWeight: 'bold', fontSize: 20, alignSelf: 'center'}}>
            THÔNG TIN ỨNG DỤNG
          </Text>
        </Modal>*/}
      </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    navigate: (route) => dispatch(navigate(route)),
    getData: () => dispatch(getData()),
    getConfig: () => dispatch(getConfig()),
    logout: () => dispatch(logout()),
  };
}

function mapStateToProps(state) {
  return {
    authStatus: state.user.authStatus,
    syncStatus: state.user.syncStatus,
    cemeteries: state.user.cemeteries,
    captured: state.user.captured,
    cemeteryCaptured: state.user.cemeteryCaptured,
    newTombs: state.user.newTombs,
    updateTombs: state.user.updateTombs,
    updateCemeteries: state.user.updateCemeteries,
  };
}

export default connect(mapStateToProps, bindAction)(HomePage);
