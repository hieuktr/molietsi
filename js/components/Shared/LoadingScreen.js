
import React, { Component } from 'react';
import { View, Image, StatusBar, Text } from 'react-native';

const logo = require('../../../images/logo-full.jpg');

const styles = {
  container: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  logo: {
    width: 300,
    height: 300,
  }
}

export default class LoadingPage extends Component {

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="white"/>
        <Image style={styles.logo} source={logo}/>
      </View>
    );
  }
}
