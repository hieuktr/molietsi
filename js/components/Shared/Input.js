
import React, { Component } from 'react';
import { 
  StyleSheet,
  View,
  Text,
  TextInput,
  Platform,
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
  },
  label: {
    color: "#999",
    fontSize: 14,
  },
  input: {
    color: "#555",
    height: 50,
    borderBottomWidth: Platform.OS === 'ios' ? 1 : 0,
    borderColor: '#f2f2f2'
  },
});

export default class Input extends Component {
  render() {
    const { label, value, onChange, secureTextEntry } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.label}>{label}</Text>
        <TextInput 
          {...this.props}
          ref={c => this._input = c }
          value={value}
          onChangeText={text => onChange(text)} 
          style={styles.input} 
          underlineColorAndroid="#E2E2E2"
          secureTextEntry={secureTextEntry}/>
      </View>
    );
  }
}
