
import React, { Component } from 'react';
import { TouchableOpacity, Text } from 'react-native';

const styles = {
  btn: {
    alignSelf: 'center',
    paddingHorizontal: 25,
    paddingVertical: 10,
    borderRadius: 3,
  },
  btnText: {
    fontSize: 18,
  }
}

export default class FlatButton extends Component {

  onPress() {
    this.props.onPress();
  }

  render() {
    return (
      <TouchableOpacity 
        style={{
          ...styles.btn, 
          backgroundColor: this.props.backgroundColor || '#FCB71E', 
          ...(this.props.disabled ? {backgroundColor: this.props.disabledBackgroundColor || "rgba(255,183,30,0.4)"} : {})
        }} 
        onPress={() => this.onPress()}
        disabled={this.props.disabled}>
        <Text style={{
          ...styles.btnText,
          ...(this.props.disabled ? {color: this.props.disabledColor || "#FCB71E"} : {color: this.props.color || '#FCB71E'})
        }}>{this.props.children}</Text>
      </TouchableOpacity>
    )
  }
}
