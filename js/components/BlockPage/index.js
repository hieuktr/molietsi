
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, FlatList, TouchableOpacity, Text, Dimensions, Picker, Alert, Platform } from 'react-native';
import Swiper from 'react-native-swiper'
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modalbox';
import { v4 as uuid } from 'uuid';

import FlatButton from '../Shared/FlatButton';
import Input from '../Shared/Input';
import { navigate } from '../../actions/nav';
import { setNewTombs, setUpdateTombs, removeNewTombs } from '../../actions/user';
import { alertMe } from '../../utils';

const styles = {
  modal: {
    width: Dimensions.get('window').width - 40,
    marginTop: Platform.OS === 'android' ? 100 : 20,
    borderRadius: 10,
    padding: 30
  },
}

function betweenString(x, y) {
  let z = "";
  if (x.length > y.length) {
		return x + "1";
	}
	for (i = 0; i < x.length; i++) {
		if (x[i] < y[i]) {
			z = x.slice(0, i+1);
			break;
		}
	}

	if (z.length == 0) {
		z = x;
	}

	z = z + "0".repeat(y.length - x.length) + "1";

	return z
}

class BlockPage extends Component {
  
  constructor(props) {
    super(props);
    
    this.state = {
      selectingRow: null,
      selectingTomb: null,
      relativePostion: 'after',
      createWithRow: 'same',
      newTombName: '',
    }
  }
  
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.title.slice(0, 20),
    headerRight: (
      <View style={{flexDirection: 'row'}}>
        {navigation.state.params.removeIcon ? (
          <TouchableOpacity
            onPress={() => navigation.state.params.removeIcon()}
          >
            <Icon name="ios-trash" size={30} style={{color: 'white', paddingHorizontal: 20}}/>
          </TouchableOpacity>
        ) : null}
        <TouchableOpacity
          onPress={() => navigation.state.params.openAddTombModal()}
        >
          <Icon name="ios-add" size={30} style={{color: 'white', paddingHorizontal: 20}}/>
        </TouchableOpacity>
      </View>
    ),
  });
  
  componentWillMount() {
    this.props.navigation.setParams({
      openAddTombModal: alertMe.bind(this, "Để thêm mới mộ, vui lòng chọn mộ liền kề với mộ mới.")
    });
  }
  
  sortTombs(tombs) {
    const { updateTombs, newTombs } = this.props;
    
    let sortedTombs = tombs
                        .map(tomb => updateTombs[tomb.id] || tomb)
                        .concat(Object.keys(newTombs)
                          .map(tombId => newTombs[tombId])
                          .filter(tomb => tomb.row === tombs[0].row && 
                                          tomb.block === tombs[0].block && 
                                          tomb.region === tombs[0].region &&
                                          tomb.cemeteryId === tombs[0].cemeteryId &&
                                          tombs[0] &&
                                          tomb !== tombs[0])
                        )
    
    sortedTombs.sort((a, b) => {
      if (a.no === b.no) {
        return a.id - b.id;
      }
      if (a.no > b.no) {
        return 1;
      }
      return -1;
    });
    return sortedTombs;
  }
  
  onRowPress(rowName) {
    this.setState({
      selectingRow: rowName === this.state.selectingRow ? null : rowName
    });
  }
  
  onTombPress(selectingTomb) {
    this.setState({
      selectingTomb: selectingTomb === this.state.selectingTomb ? null : selectingTomb
    });
    
    if (selectingTomb !== this.state.selectingTomb) {
      this.props.navigation.setParams({
        openAddTombModal: this.refs.addTombModal.open,
        removeIcon: selectingTomb._new ? () => {
          Alert.alert(
            'Xoá mộ vừa tạo',
            'Bạn có chắc chắn?',
            [
              {text: 'Huỷ', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'Xoá', onPress: () => {console.log(selectingTomb); this.props.removeNewTombs([selectingTomb.id])}},
            ]
          );
        } : null
      });
    } else {
      this.props.navigation.setParams({
        openAddTombModal: alertMe.bind(this, "Để thêm mới mộ, vui lòng chọn mộ liền kề với mộ mới.")
      });
    }
  }
  
  onNextPress() {
    this.props.navigate({routeName: "tomb_page", params: {tomb: this.state.selectingTomb}});
  }
  
  addTomb() {
    const { relativePostion, createWithRow, newTombName } = this.state;
    const { cemeteryId, region, block } = this.props.navigation.state.params;
    
    let selectingTomb = this.state.selectingTomb;
    if (this.props.updateTombs[selectingTomb.id]) {
      selectingTomb = {...this.props.updateTombs[selectingTomb.id]};
    }
    const rows = this.getRows();
    let newTomb = {
      id: uuid(),
      _new: true,
      fullname: newTombName,
      cemeteryId,
      region,
      block,
    };
    
    if (createWithRow === "same") {
      if (selectingTomb) {
        const tombsInSelectingRow = this.sortTombs(rows[selectingTomb.row]);
        const selectingTombIndex = tombsInSelectingRow.map(tomb => tomb.id).indexOf(selectingTomb.id);
        newTomb.row = selectingTomb.row;
        
        if (relativePostion === "before") {
          const beforeSelectingTomb = selectingTombIndex === 0 ? null : tombsInSelectingRow[parseInt(selectingTombIndex) - 1];
          if (!beforeSelectingTomb) {
            newTomb.no = "-"+selectingTomb.no;
          } else {
            newTomb.no = betweenString(beforeSelectingTomb.no, selectingTomb.no);
          }
        } else {
          const afterSelectingTomb = selectingTombIndex === tombsInSelectingRow.length - 1 ? null : tombsInSelectingRow[parseInt(selectingTombIndex) + 1];
          if (!afterSelectingTomb) {
            newTomb.no = selectingTomb.no+"a";
          } else {
            newTomb.no = betweenString(selectingTomb.no, afterSelectingTomb.no);
          }
        }
      }
    } else {
      newTomb.row = Object.keys(rows).sort()[Object.keys(rows).length - 1] + "a";
      newTomb.no = "1";
    }
    
    this.props.setNewTombs([newTomb]);
    // this.props.setUpdateTombs(processingTombs.filter(tomb => !tomb._new));
    this.refs.addTombModal.close();
  }
  
  havePermission(tomb) {
    return this.props.profile.id === tomb.managedBy || !tomb.managedBy;
  }
  
  getRows() {
    const { cemeteryId, region, block } = this.props.navigation.state.params;
    const rows = {...this.props.tombsByCemetery[cemeteryId][region][block]};
    for (let tombId in this.props.newTombs) {
      const tomb = this.props.newTombs[tombId];
      if (!rows[tomb.row] && tomb.cemeteryId === cemeteryId && tomb.region === region && tomb.block === block) {
        rows[tomb.row] = [tomb];
      }
    }
    return rows;
  }

  render() {
    const rows = this.getRows();
    const { captured } = this.props;
    const { selectingRow, selectingTomb, relativePostion, createWithRow, newTombName } = this.state;
    
    return (
      <View style={{flex: 1}}>
        {rows ? (
          <FlatList 
            // extraData={selectingTomb}
            data={Object.keys(rows).sort()}
            keyExtractor={item => item+""}
            renderItem={({item: rowName, index}) => {
              return (
                <View style={{flex: 1}}>
                  <TouchableOpacity 
                    onPress={() => this.onRowPress(rowName)}
                    style={{
                      backgroundColor: '#b2b2b2',
                      padding: 10,
                      marginBottom: 1,
                    }}
                    activeOpacity={0.7}>
                    <Text style={{ fontSize: 18, color: 'white', fontWeight: 'bold' }}>
                      Hàng {rowName} ({this.sortTombs(rows[rowName]).length} phần mộ)
                    </Text>
                  </TouchableOpacity>
                  {selectingRow === rowName || rows[rowName].length <= 10 || Object.keys(rows).length === 1 ? (
                    <FlatList 
                      // extraData={selectingTomb}
                      style={{margin: 10, marginTop: 1}}
                      data={this.sortTombs(rows[rowName])}
                      keyExtractor={(item, index) => index+""}
                      renderItem={({item: tomb, index}) => {
                        return (
                          <TouchableOpacity 
                            onPress={() => this.onTombPress(tomb)}
                            style={{
                              backgroundColor: selectingTomb && selectingTomb.id === tomb.id ? (
                                '#7A92A9'
                              ) : (
                                (tomb.photoMain && tomb.photoPortrait && tomb.photoLandscape) ||
                                (captured && 
                                captured[tomb.id] && 
                                captured[tomb.id].main && captured[tomb.id].main.uri &&
                                captured[tomb.id].portrait && captured[tomb.id].portrait.uri &&
                                captured[tomb.id].landscape && captured[tomb.id].landscape.uri) ? '#88CC88' : 'white'
                              ),
                              paddingHorizontal: 15,
                              marginBottom: 2,
                              flexDirection: 'row',
                              alignItems: 'center',
                              ...(tomb._new ? {
                                borderColor: 'orange',
                                borderWidth: 3
                              } : {}),
                              ...(tomb.wrongMartyr || tomb.wrongProps || tomb.noTomb || tomb.noName || tomb.note ? {
                                borderColor: 'red',
                                borderWidth: 3
                              } : {})
                            }}
                            activeOpacity={0.7}>
                            <View style={{paddingVertical: 15, flex: 1}}>
                              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                {this.havePermission(tomb) ? null : <Icon name="ios-eye" style={{marginRight: 10}} size={18}/>}
                                <Text style={{fontSize: 12}}>Id. {tomb.id}</Text>
                              </View>
                              <Text style={{ fontSize: 18 }}>
                                {tomb.no}. LS {tomb.fullname || "Chưa biết tên"}
                              </Text>
                            </View>
                            {selectingTomb && selectingTomb.id === tomb.id ? (
                              <TouchableOpacity onPress={() => this.onNextPress()}>
                                <Icon name={this.havePermission(tomb) ? "ios-camera" : "ios-arrow-round-forward-outline"} size={40} style={{color: 'white', paddingHorizontal: 20}} />
                              </TouchableOpacity>
                            ) : null}
                            <View style={{position: 'absolute', top: 5, right: 5, flexDirection: 'row'}}>
                              {tomb.photoMain || (captured && captured[tomb.id] && captured[tomb.id].main && captured[tomb.id].main.uri) ? (
                                <Icon name="ios-checkmark-circle" size={15} style={{color: 'green', marginLeft: 3}}/>
                              ) : (
                                <Icon name="ios-close-circle" size={15} style={{color: 'red', marginLeft: 3}}/>
                              )}
                              {tomb.photoPortrait || (captured && captured[tomb.id] && captured[tomb.id].portrait && captured[tomb.id].portrait.uri) ? (
                                <Icon name="ios-checkmark-circle" size={15} style={{color: 'green', marginLeft: 3}}/>
                              ) : (
                                <Icon name="ios-close-circle" size={15} style={{color: 'red', marginLeft: 3}}/>
                              )}
                              {tomb.photoLandscape || (captured && captured[tomb.id] && captured[tomb.id].landscape && captured[tomb.id].landscape.uri) ? (
                                <Icon name="ios-checkmark-circle" size={15} style={{color: 'green', marginLeft: 3}}/>
                              ) : (
                                <Icon name="ios-close-circle" size={15} style={{color: 'red', marginLeft: 3}}/>
                              )}
                            </View>
                          </TouchableOpacity>
                        );
                      }}
                    />
                  ) : null}
                </View>
              );
            }}
          />
        ) : (
            <Text style={{fontSize: 17, lineHeight: 30, alignSelf: 'center', marginTop: 20}}>
              Không có phần mộ nào.
            </Text>
        )}
        
        <Modal 
          style={{...styles.modal, height: createWithRow === "same" ? (Platform.OS === 'android' ? 380 : 510) : 310}} 
          backButtonClose={true} 
          position={"top"} 
          ref={"addTombModal"}>
          <Text style={{fontWeight: 'bold', fontSize: 20, alignSelf: 'center', marginBottom: Platform.OS === 'android' ? 20 : 0}}>
            THÊM PHẦN MỘ MỚI
          </Text>
          <Picker
            selectedValue={createWithRow}
            style={Platform.OS === 'android' ? {
              height: 50, marginBottom: 20
            } : {
              height: 110, marginBottom: 30, marginTop: -30 
            }}
            onValueChange={(itemValue, itemIndex) => this.setState({createWithRow: itemValue})}>
            <Picker.Item label="Cùng hàng" value="same" />
            <Picker.Item label="Hàng mới" value="new" />
          </Picker>
          {createWithRow === "same" ? (
            <Picker
              selectedValue={relativePostion}
              style={Platform.OS === 'android' ? {
                height: 50, marginBottom: 30
              } : {
                height: 110, marginBottom: 75
              }}
              onValueChange={(itemValue, itemIndex) => this.setState({relativePostion: itemValue})}>
              <Picker.Item label="Ở Sau mộ đang chọn" value="after" />
              <Picker.Item label="Ở Trước mộ đang chọn" value="before" />
            </Picker>
          ) : null}
          <Input
            label="Họ và tên"
            value={newTombName}
            onChange={newTombName => this.setState({ newTombName })} 
            autoCapitalize="characters"
          />
          <FlatButton
            onPress={() => this.addTomb()}
          >
            <Text style={{color: 'white'}}>THÊM</Text>
          </FlatButton>
        </Modal>
      </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    navigate: (route) => dispatch(navigate(route)),
    setNewTombs: (tombs) => dispatch(setNewTombs(tombs)),
    setUpdateTombs: (tombs) => dispatch(setUpdateTombs(tombs)),
    removeNewTombs: (tombIds) => dispatch(removeNewTombs(tombIds)),
  };
}

function mapStateToProps(state) {
  return {
    profile: state.user.profile,
    newTombs: state.user.newTombs,
    updateTombs: state.user.updateTombs,
    captured: state.user.captured,
    tombsByCemetery: state.user.tombsByCemetery,
  };
}

export default connect(mapStateToProps, bindAction)(BlockPage);
