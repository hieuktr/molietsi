
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, FlatList, TouchableOpacity, Text, ImageBackground, Dimensions, Platform } from 'react-native';
import Swiper from 'react-native-swiper'
import Icon from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-picker';
import Permissions from 'react-native-permissions';
import RNFS from 'react-native-fs';

import { navigate } from '../../actions/nav';
import { setCemeteryCaptured } from '../../actions/user';
import { alertMe } from '../../utils';
import { DEFAULT_CDN_URL } from '../../constants';

const styles = {
  wrapper: {
    height: 180,
    width: Dimensions.get('window').width,
    backgroundColor: 'white'
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
    alignItems: 'center'
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  },
  image: {
    flex: 1,
    width: Dimensions.get('window').width,
  },
  paginationStyle: {
    position: 'absolute',
    bottom: 10,
    right: 10
  },
  paginationText: {
    fontSize: 20
  },
  footerTab: {
    backgroundColor: '#FCB71E',
    flexDirection: 'row',
    padding: 10
  }
}

const renderPagination = (index, total, context) => {
  return (
    <View style={styles.paginationStyle}>
      <Text style={{ color: 'grey' }}>
        <Text style={styles.paginationText}>{index + 1}</Text>/{total}
      </Text>
    </View>
  )
}

class CemeteryPage extends Component {
  
  constructor(props) {
    super(props);
    const cemetery = props.navigation.state.params.cemetery;
    this.state = {
      selectingRegion: props.tombsByCemetery[cemetery.id] && 
        Object.keys(props.tombsByCemetery[cemetery.id]).length > 0 ? 
        Object.keys(props.tombsByCemetery[cemetery.id])[0] : null,
    }
  }
  
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.cemetery.name,
    headerRight: (
      <TouchableOpacity
        onPress={() => navigation.state.params.captureCemetery ? navigation.state.params.captureCemetery() : null}
      >
        <Icon name="ios-camera" size={30} style={{color: 'white', paddingHorizontal: 20}}/>
      </TouchableOpacity>
    ),
  });
  
  componentWillMount() {
    this.requestCameraPermission();
  }
  
  async requestCameraPermission() {
    await Permissions.request('camera', {
      rationale: {
        title: 'Quyền truy cập Camera',
        message: 'Mộ Liệt Sĩ cần quyền camera để chụp ảnh nghĩa trang.',
      },
    }).then(response => {
      // Returns once the user has chosen to 'allow' or to 'not allow' access
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      if (response === 'authorized') {

      } else {
        alertMe("Ứng dụng không được cấp quyền truy cập camera.");
      }
    });
  }
  
  componentDidMount() {
    this.props.navigation.setParams({
      captureCemetery: this.captureCemetery.bind(this)
    });
  }
  
  captureCemetery() {
    const options = {
      title: "Ảnh mộ",
      // maxHeight: 2000,
      takePhotoButtonTitle: "Chụp mới",
      chooseFromLibraryButtonTitle: "Chọn từ thư viện",
      // quality: 0.5,
      // rotation	-	OK	Photos only, 0 to 360 degrees of rotation
      noData: true,
      storageOptions: {
        skipBackup: true,
        path: 'molietsi',
        // storageOptions.cameraRoll	OK	OK	If true, the cropped photo will be saved to the iOS Camera Roll or Android DCIM folder.
        cameraRoll: false,
      }
    };
    
    ImagePicker.launchCamera(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        const cemetery = this.props.navigation.state.params.cemetery;
        
        const cemeteryImages = this.props.cemeteryCaptured && 
                                this.props.cemeteryCaptured[cemetery.id] && 
                                typeof this.props.cemeteryCaptured[cemetery.id] === 'object' ? 
                                this.props.cemeteryCaptured[cemetery.id] : {};
        this.props.setCemeteryCaptured(cemetery.id, 
          {
            ...cemeteryImages,
            [Date.now()]: {
              uri: Platform.OS === 'android' ? `file://${response.path}` : response.uri.split("/").slice(-1)[0],
              width: response.width,
              height: response.height,
            }
          }
        );
      }
    });
  }
  
  onRegionPress(regionName) {
    this.setState({
      selectingRegion: regionName === this.state.selectingRegion ? null : regionName
    });
  }
  
  onBlockPress(blockName) {
    this.props.navigate({routeName: "block_page", params: {
      title: `Khu ${this.state.selectingRegion} - Lô ${blockName}`,
      cemeteryId: this.props.navigation.state.params.cemetery.id,
      region: this.state.selectingRegion,
      block: blockName,
    }});
  }
  
  getRows(inputRows, cemeteryId, region, block) {
    const rows = {...inputRows};
    for (let tombId in this.props.newTombs) {
      const tomb = this.props.newTombs[tombId];
      if (!rows[tomb.row] && tomb.cemeteryId === cemeteryId && tomb.region === region && tomb.block === block) {
        rows[tomb.row] = [tomb];
      }
    }
    return rows;
  }

  render() {
    const { tombsByCemetery, cemeteryCaptured, navigate, profile, cdnUrl } = this.props;
    const { selectingRegion } = this.state;
    
    const cemetery = this.props.navigation.state.params.cemetery;
    const cemeteryImages = (cemetery && cemetery.photos ? 
                            cemetery.photos.map(photo => ({
                              uri: `${cdnUrl || DEFAULT_CDN_URL}/400x300/${photo}`,
                              width: 400,
                              height: 300,
                            })) : [])
                              .concat(
                                cemeteryCaptured && 
                                cemeteryCaptured[cemetery.id] &&
                                typeof cemeteryCaptured[cemetery.id] === 'object' ? 
                                Object.keys(cemeteryCaptured[cemetery.id]).map(k => cemeteryCaptured[cemetery.id][k]) : []
                              );
    
    const regions = tombsByCemetery[cemetery.id];
    
    return (
      <View style={{flex: 1}}>
        {regions ? (
          <FlatList 
            style={{flex: 1}}
            data={["swiper"].concat(Object.keys(regions))}
            keyExtractor={item => item}
            renderItem={({item: regionName, index}) => {
              if (regionName === "swiper") {
                return (
                  <View style={styles.wrapper}>
                    {cemeteryImages.length > 0 ? (
                      <Swiper
                        renderPagination={(index, total, context) => (
                          <View style={styles.paginationStyle}>
                            <Text style={{ color: 'grey' }}>
                              <Text style={styles.paginationText}>{index + 1}</Text>/{total}
                            </Text>
                          </View>
                        )}
                      >
                      {cemeteryImages.map(image => (
                        <View style={styles.slide} key={image.uri}>
                          <ImageBackground resizeMode="stretch" style={styles.image} source={{ uri: Platform.OS === 'ios' ? `${RNFS.DocumentDirectoryPath}/molietsi/${image.uri}` : image.uri, width: image.width, height: image.height }} />
                        </View>
                      ))}
                      </Swiper>
                    ) : (
                      <TouchableOpacity onPress={() => this.captureCemetery()} style={{...styles.slide, alignItems: 'center'}}>
                        <Icon name="ios-image" size={50} />
                        <Text>Ảnh nghĩa trang</Text>
                      </TouchableOpacity>
                    )}
                  </View>
                )
              }
              return (
                <View style={{flex: 1}}>
                  <TouchableOpacity 
                    onPress={() => this.onRegionPress(regionName)}
                    style={{
                      margin: 10,
                      backgroundColor: 'white',
                      padding: 20,
                      marginBottom: 10,
                    }}
                    activeOpacity={0.7}>
                    <Text style={{ fontSize: 18 }}>
                      Khu {regionName} ({Object.keys(regions[regionName]).length} lô)
                    </Text>
                  </TouchableOpacity>
                  {selectingRegion === regionName ? (
                    <FlatList 
                      style={{marginLeft: 40, marginRight: 10}}
                      data={Object.keys(regions[regionName])}
                      keyExtractor={item => item}
                      renderItem={({item: blockName, index}) => {
                        return (
                          <TouchableOpacity 
                            onPress={() => this.onBlockPress(blockName)}
                            style={{
                              backgroundColor: 'white',
                              paddingVertical: 10,
                              paddingHorizontal: 30,
                              marginBottom: 10,
                            }}
                            activeOpacity={0.7}>
                            <Text style={{ fontSize: 18 }}>
                              Lô {blockName} ({Object.keys(this.getRows(regions[regionName][blockName], cemetery.id, regionName, blockName)).length} hàng)
                            </Text>
                          </TouchableOpacity>
                        );
                      }}
                    />
                  ) : null}
                </View>
              );
            }}
          />
        ) : (
            <Text style={{fontSize: 17, lineHeight: 30, alignSelf: 'center', marginTop: 20, flex: 1}}>
              Không có khu nghĩa trang nào.
            </Text>
        )}
        {profile.role === "admin" || profile.role === "operator" || profile.role === "coordinator" ? (
          <View style={styles.footerTab}>
            <TouchableOpacity
              style={{flex: 1, alignItems: 'center'}}
              onPress={() => navigate({routeName: "cemetery_location_page", params: {cemetery}})}
            >
              <Text style={{color: 'white', fontSize: 16}}>
                Cập nhật Vị trí
              </Text>
              <Icon name="ios-pin" size={30} style={{color: 'white'}}/>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    navigate: (route) => dispatch(navigate(route)),
    setCemeteryCaptured: (cemeteryId, images) => dispatch(setCemeteryCaptured(cemeteryId, images)),
  };
}

function mapStateToProps(state) {
  return {
    cdnUrl: state.user.config ? state.user.config.cdnUrl : DEFAULT_CDN_URL,
    profile: state.user.profile,
    tombsByCemetery: state.user.tombsByCemetery,
    cemeteryCaptured: state.user.cemeteryCaptured,
    newTombs: state.user.newTombs,
  };
}

export default connect(mapStateToProps, bindAction)(CemeteryPage);
