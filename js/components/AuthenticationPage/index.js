
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'react-native';
import { NavigationActions } from 'react-navigation';

import LoadingScreen from '../Shared/LoadingScreen'
import FlatButton from '../Shared/FlatButton';
import Input from '../Shared/Input';

import { login } from '../../actions/user';
import { resetRoute } from '../../actions/nav';

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20,
    paddingTop: 50
  },
  title: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#FCB71E',
    position: 'absolute',
    bottom: 20
  },
  subtitle: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 30
  },
  error: {
    color: 'red',
    alignSelf: 'center',
    marginTop: 30,
  }
}

class AuthenticationPage extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      username: props.profile.username,
      password: ""
    }
  }

  componentWillMount() {
    const { resetRoute, token } = this.props;
    console.log("Token:", token);
    if (token) {
      resetRoute('home_page');
    }
  }
  
  onLogin() {
    this.props.login(this.state.username, this.state.password);
  }

  render() {
    const { authStatus, authError, profile } = this.props;
    const { username, password } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.subtitle}>{!profile.username ? "ĐĂNG NHẬP" : "VUI LÒNG ĐĂNG NHẬP LẠI"}</Text>
        <Input
          label="TÀI KHOẢN"
          value={profile.username || this.state.username}
          onChange={username => this.setState({ username })} 
          returnKeyType="next"
          autoCapitalize="none"
          onSubmitEditing={() => { this.refs.password._input.focus(); }}
          blurOnSubmit={false}
        />
        <Input
          label="MẬT KHẨU"
          value={this.state.password}
          onChange={password => this.setState({ password })} 
          secureTextEntry={true}
          returnKeyType="next"
          autoCapitalize="none"
          ref="password"
        />
        <FlatButton
          disabled={!username || !password || authStatus === "loading"} 
          onPress={() => this.onLogin()}>
          <Text style={{color: 'white'}}>{authStatus === "loading" ? "ĐANG KẾT NỐI..." : "ĐĂNG NHẬP"}</Text>
        </FlatButton>
        {authStatus === "failed" ? <Text style={styles.error}>{authError}</Text> : null}
        <Text style={styles.title}>MỘ LIỆT SĨ</Text>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    token: state.user.token,
    profile: state.user.profile,
    authStatus: state.user.authStatus,
    authError: state.user.authError,
  };
}

function bindActions(dispatch) {
  return {
    login: (username, password) => dispatch(login(username, password)),
    resetRoute: route => dispatch(resetRoute(route)),
  };
}

export default connect(mapStateToProps, bindActions)(AuthenticationPage);
