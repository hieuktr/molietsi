
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, Text, Dimensions, Picker, Platform, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modalbox';
import Permissions from 'react-native-permissions';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

import FlatButton from '../Shared/FlatButton';
import Input from '../Shared/Input';

import { setUpdateCemetery } from '../../actions/user';
import { alertMe } from '../../utils';

const styles = {
  modal: {
    width: Dimensions.get('window').width - 40,
    marginTop: 100,
    borderRadius: 10,
    padding: 30
  },
}

const CEMETERY_LOCATIONS = {
  'statue': 'Tượng đài',
  'gate': 'Cổng',
  'bathuong': 'Bát hương lớn',
  'corner1': 'Góc 1',
  'corner2': 'Góc 2',
  'corner3': 'Góc 3',
  'corner4': 'Góc 4',
}

class CemeteryLocationPage extends Component {
  
  _watchID: ?number = null;
  
  constructor(props) {
    super(props);
    this.state = {
      position: null,
      locationName: 'gate'
    }
  }
  
  static navigationOptions = ({ navigation }) => ({
    title: `Vị trí ${navigation.state.params.cemetery.name}`.slice(0, 30),
    headerRight: (
      <TouchableOpacity
        onPress={() => navigation.state.params.openAddLocationModal()}
      >
        <Icon name="ios-add" size={30} style={{color: 'white', paddingHorizontal: 20}}/>
      </TouchableOpacity>
    ),
  });
  
  componentWillMount() {
    this.requestLocationPermission();
  }
  
  componentDidMount() {
    this.props.navigation.setParams({
      openAddLocationModal: this.refs.addLocationModal.open,
    });
  }
  
  componentWillUnmount() {
    try {
      navigator.geolocation.clearWatch(this._watchID);
    } catch(e) {}
  }
  
  async requestLocationPermission() {
    await Permissions.request('location', {
      rationale: {
        title: 'Quyền truy cập Vị trí',
        message:
          'Mộ Liệt Sĩ cần quyền lấy vị trí để lưu các địa điểm trong nghĩa trang.',
      },
    }).then(response => {
      // Returns once the user has chosen to 'allow' or to 'not allow' access
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      console.log("quyen truy cap", response);
      if (response === 'authorized') {
        this.getLocationUpdate();
      } else {
        alertMe("Ứng dụng không được cấp quyền truy cập vị trí.");
        console.log("Ứng dụng không được cấp quyền truy cập vị trí.", response);
      }
    });
  }
  
  async getLocationUpdate() {
    if (Platform.OS === 'android') {
      await LocationServicesDialogBox.checkLocationServicesIsEnabled({
        message: "Bạn chưa bật định vị. Mộ Liệt Sĩ cần lấy vị trí để lưu các địa điểm trong nghĩa trang.",
        ok: "Đồng ý",
        cancel: "Huỷ",
        enableHighAccuracy: false, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
        showDialog: true, // false => Opens the Location access page directly
        openLocationServices: true, // false => Directly catch method is called if location services are turned off
        preventOutSideTouch: true, //true => To prevent the location services window from closing when it is clicked outside
        preventBackClick: true //true => To prevent the location services popup from closing when it is clicked back button
      }).then(function(success) {
        console.log(success); // success => {alreadyEnabled: false, enabled: true, status: "enabled"}
        
      }).catch((error) => {
        console.log(error.message); // error.message => "disabled"
      });
    }
      
    try {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          // var initialPosition = JSON.stringify(position);
          try {
            console.log(position);
            if (position && position.coords) {
              this.setState({ 
                position: {
                  lat: position.coords.latitude,
                  long: position.coords.longitude
                }
              });
            }
          } catch(e){
            console.log(e);
          }
        },
        (error) => console.log(JSON.stringify(error)),
        // {enableHighAccuracy: true, timeout: 5000, maximumAge: 0}
      );
      this._watchID = navigator.geolocation.watchPosition(
        (position) => {
          // var lastPosition = JSON.stringify(position);
          console.log(position);
          if (position && position.coords) {
            this.setState({ 
              position: {
                lat: position.coords.latitude,
                long: position.coords.longitude
              }
            });
          }
        },
        (error) => console.log(JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 5000, maximumAge: 0, distanceFilter: 1}
      );
    } catch(e) {console.log(e)}
  }
  
  addLocation() {
    const { position, locationName } = this.state;
    if (position) {
      const { updateCemeteries } = this.props;
      const cemetery = this.props.navigation.state.params.cemetery;
      const locations = {};
      if (cemetery.locations) {
        for (let location of cemetery.locations) {
          locations[location.position] = {...location};
        }
      }
      this.props.setUpdateCemetery({
        id: cemetery.id,
        locations: {
          ...locations,
          ...(updateCemeteries[cemetery.id] ? updateCemeteries[cemetery.id].locations : {}),
          [locationName]: position
        }
      });
    }
    this.refs.addLocationModal.close();
  }
  
  render() {
    const { updateCemeteries } = this.props;
    const { locationName, position } = this.state;
    const cemetery = this.props.navigation.state.params.cemetery;
    const locations = {};
    if (cemetery.locations) {
      for (let location of cemetery.locations) {
        locations[location.position] = {
          lat: location.lat,
          long: location.long,
        };
      }
    }
    const cemeteryLocation = {
      ...locations,
      ...(updateCemeteries[cemetery.id] ? updateCemeteries[cemetery.id].locations : {})
    }
    console.log("cemetery", cemetery);
    
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <ScrollView>
          {Object.keys(cemeteryLocation).map(locationName => {
            return (
              <View key={locationName} style={{padding: 20}}>
                <Text style={{fontSize: 18, fontWeight: 'bold'}}>{CEMETERY_LOCATIONS[locationName]}</Text>
                <Text>{cemeteryLocation && cemeteryLocation[locationName] ? `${cemeteryLocation[locationName].lat}, ${cemeteryLocation[locationName].long}` : "Không xác định"}</Text>
              </View>
            )
          })}
        </ScrollView>
        <Modal 
          style={{...styles.modal, height: Platform.OS === 'android' ? 300 : 400 }} 
          backButtonClose={true} 
          position={"top"} 
          ref={"addLocationModal"}>
          <Text style={{fontWeight: 'bold', fontSize: 20, alignSelf: 'center', marginBottom: Platform.OS === 'android' ? 20 : 5}}>
            THÊM VỊ TRÍ
          </Text>
          <Picker
            selectedValue={locationName}
            style={{ height: Platform.OS === 'android' ? 50 : 200, marginBottom: 20 }}
            onValueChange={(itemValue, itemIndex) => this.setState({locationName: itemValue})}>
            <Picker.Item label="Cổng" value="gate" />
            <Picker.Item label="Tượng đài" value="statue" />
            <Picker.Item label="Bát hương lớn" value="bathuong" />
            <Picker.Item label="Góc 1" value="corner1" />
            <Picker.Item label="Góc 2" value="corner2" />
            <Picker.Item label="Góc 3" value="corner3" />
            <Picker.Item label="Góc 4" value="corner4" />
          </Picker>
          <Text style={{marginBottom: 30}}>
            {`Toạ độ hiện tại: ${position ? `${position.lat}, ${position.long}` : "Không xác định"}`}
          </Text>
          <FlatButton
            onPress={() => this.addLocation()}
          >
            <Text style={{color: 'white'}}>ĐẶT TOẠ ĐỘ HIỆN TẠI</Text>
          </FlatButton>
        </Modal>
      </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    setUpdateCemetery: (cemetery) => dispatch(setUpdateCemetery(cemetery)),
  };
}

function mapStateToProps(state) {
  return {
    updateCemeteries: state.user.updateCemeteries,
  };
}

export default connect(mapStateToProps, bindAction)(CemeteryLocationPage);
