
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, Text, Image, Dimensions, ImageBackground, ScrollView, TextInput, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-picker';
import Permissions from 'react-native-permissions';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import RNFS from 'react-native-fs';

import { navigate, goBack } from '../../actions/nav';
import { setNewTombs, setUpdateTombs, setCaptured } from '../../actions/user';
import { DEFAULT_CDN_URL } from '../../constants';
import { alertMe } from '../../utils';

import { Client } from 'bugsnag-react-native';
const bugsnag = new Client();

const smallImageWidth = (Dimensions.get('window').width-30)/2;
const smallImageHeight = smallImageWidth/3*4;

const landscapeImageWidth = Dimensions.get('window').width-20;
const landscapeImageHeight = landscapeImageWidth/4*3;

const styles = {
  imageWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  image: {
    width: smallImageWidth,
    height: smallImageHeight,
  },
  landscapeImage: {
    width: landscapeImageWidth,
    height: landscapeImageHeight,
  },
  text: {
    fontSize: 16,
  }
}

class TombPage extends Component {
  
  _watchID: ?number = null;
  capturing = false;
  
  state = {
    position: {},
    isImageViewVisible: false,
    imageViewIndex: 0,
  }
  
  async getLocationUpdate() {
    if (Platform.OS === 'android') {
      await LocationServicesDialogBox.checkLocationServicesIsEnabled({
        message: "Bạn chưa bật định vị. Mộ Liệt Sĩ cần lấy vị trí để lưu các địa điểm trong nghĩa trang.",
        ok: "Đồng ý",
        cancel: "Huỷ",
        enableHighAccuracy: false, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
        showDialog: true, // false => Opens the Location access page directly
        openLocationServices: true, // false => Directly catch method is called if location services are turned off
        preventOutSideTouch: true, //true => To prevent the location services window from closing when it is clicked outside
        preventBackClick: true //true => To prevent the location services popup from closing when it is clicked back button
      }).then(function(success) {
        console.log(success); // success => {alreadyEnabled: false, enabled: true, status: "enabled"}
      }).catch((error) => {
        console.log(error.message); // error.message => "disabled"
      });
    }
    
    try {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          // var initialPosition = JSON.stringify(position);
          try {
            if (position && position.coords) {
              this.setState({ 
                position: {
                  lat: position.coords.latitude,
                  long: position.coords.longitude
                }
              });
            }
          } catch(e){
            console.log(e);
          }
        },
        (error) => console.log(JSON.stringify(error)),
        // {enableHighAccuracy: true, timeout: 5000, maximumAge: 0}
      );
      this._watchID = navigator.geolocation.watchPosition(
        (position) => {
          // var lastPosition = JSON.stringify(position);
          if (position && position.coords) {
            this.setState({ 
              position: {
                lat: position.coords.latitude,
                long: position.coords.longitude
              }
            });
          }
        },
        (error) => console.log(JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 5000, maximumAge: 0, distanceFilter: 1}
      );
    } catch(e) {console.log(e)}
  }
  
  componentWillMount() {
    this.requestPermissions();
  }
  
  async requestPermissions() {
    await Permissions.request('location', {
      rationale: {
        title: 'Quyền truy cập Vị trí',
        message:
          'Mộ Liệt Sĩ cần quyền lấy vị trí để lưu các địa điểm trong nghĩa trang.',
      },
    }).then(response => {
      // Returns once the user has chosen to 'allow' or to 'not allow' access
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      if (response === 'authorized') {
        this.getLocationUpdate();
      } else {
        alertMe("Ứng dụng không được cấp quyền truy cập vị trí.");
      }
    });
    
    await Permissions.request('camera', {
      rationale: {
        title: 'Quyền truy cập Camera',
        message:
          'Mộ Liệt Sĩ cần quyền camera để chụp ảnh mộ.',
      },
    }).then(response => {
      // Returns once the user has chosen to 'allow' or to 'not allow' access
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      if (response === 'authorized') {

      } else {
        alertMe("Ứng dụng không được cấp quyền truy cập camera.");
      }
    });
  }
  
  componentWillUnmount() {
    try {
      navigator.geolocation.clearWatch(this._watchID);
    } catch(e) {console.log(e)}
  }
  
  getTomb() {
    const { cdnUrl } = this.props;
    const tombId = this.props.navigation.state.params.tomb.id;
    const tomb = this.props.newTombs[tombId] || this.props.updateTombs[tombId] || this.props.navigation.state.params.tomb;

    const IMAGE_SIZE = {
      short: 576,
      long: 768,
    }
    
    let mainImage, portraitImage, landscapeImage;
    if (tomb.photoMain) {
      mainImage = {
        isSynced: true,
        uri: `${cdnUrl || DEFAULT_CDN_URL}/${IMAGE_SIZE.short}x${IMAGE_SIZE.long}/${tomb.photoMain}`,
        width: IMAGE_SIZE.short,
        height: IMAGE_SIZE.long,
      }
    }
    if (tomb.photoPortrait) {
      portraitImage = {
        isSynced: true,
        uri: `${cdnUrl || DEFAULT_CDN_URL}/${IMAGE_SIZE.short}x${IMAGE_SIZE.long}/${tomb.photoPortrait}`,
        width: IMAGE_SIZE.short,
        height: IMAGE_SIZE.long,
      }
    }
    if (tomb.photoLandscape) {
      landscapeImage = {
        isSynced: true,
        uri: `${cdnUrl || DEFAULT_CDN_URL}/${IMAGE_SIZE.long}x${IMAGE_SIZE.short}/${tomb.photoLandscape}`,
        width: IMAGE_SIZE.long,
        height: IMAGE_SIZE.short,
      }
    }
    
    const tombSavedImages = this.props.captured ? this.props.captured[tomb.id] : null;
    if (tombSavedImages) {
      if (tombSavedImages.main) {
        mainImage = {
          ...tombSavedImages.main,
          isSynced: false,
          ...(Platform.OS === 'ios' ? {uri: `${RNFS.DocumentDirectoryPath}/molietsi/${tombSavedImages.main.uri}`} : {})
        }
      }
      if (tombSavedImages.portrait) {
        portraitImage = {
          ...tombSavedImages.portrait,
          isSynced: false,
          ...(Platform.OS === 'ios' ? {uri: `${RNFS.DocumentDirectoryPath}/molietsi/${tombSavedImages.portrait.uri}`} : {})
        }
      }
      if (tombSavedImages.landscape) {
        landscapeImage = {
          ...tombSavedImages.landscape,
          isSynced: false,
          ...(Platform.OS === 'ios' ? {uri: `${RNFS.DocumentDirectoryPath}/molietsi/${tombSavedImages.landscape.uri}`} : {})
        }
      }
    }
    
    return {
      ...tomb,
      mainImage,
      portraitImage,
      landscapeImage,
    };
  }
  
  onTagClick(attr) {
    const tomb = this.getTomb();
    this.props[tomb._new ? "setNewTombs" : "setUpdateTombs"]([{
      ...tomb,
      [attr]: !tomb[attr]
    }]);
  }
  
  onNoteChange(text) {
    const tomb = this.getTomb();
    this.props[tomb._new ? "setNewTombs" : "setUpdateTombs"]([{
      ...tomb,
      note: text
    }]);
  }
  
  onCaptureImage(imageType, direct) {
    // if (!this.state.position.lat && !this.state.position.long) {
    //   alertMe('Không thể chụp ảnh do không lấy được toạ độ của máy. Vui lòng mở chức năng định vị trong Cài đặt của máy.')
    //   return;
    // }
    if (!this.capturing || direct) {
      this.capturing = true;
      
      let title = "Chụp ảnh Mộ";
      if (imageType === "main") {
        title = "Chụp ảnh Bia mộ";
      } else if (imageType === "portrait") {
        title = "Chụp ảnh Mặt trước mộ";
      } else if (imageType === "landscape") {
        title = "Chụp ảnh Toàn cảnh";
      }
      const options = {
        title,
        maxHeight: 2000,
        takePhotoButtonTitle: "Chụp mới",
        chooseFromLibraryButtonTitle: "Chọn từ thư viện",
        quality: 0.5,
        // rotation	-	OK	Photos only, 0 to 360 degrees of rotation
        noData: true,
        storageOptions: {
          skipBackup: true,
          path: 'molietsi',
          // storageOptions.cameraRoll	OK	OK	If true, the cropped photo will be saved to the iOS Camera Roll or Android DCIM folder.
          cameraRoll: false,
        }
      };
      
      ImagePicker.launchCamera(options, async (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled image picker');
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }
        else {
          const tomb = this.getTomb();
          const tombImages = this.props.captured ? this.props.captured[tomb.id] || {} : {};
          const { position } = this.state;
          
          let savedUri;
          if (Platform.OS === 'ios') {
            savedUri = response.uri.split("/").slice(-1)[0];
            console.log("savedUri", savedUri);
          } else {
            savedUri = RNFS.ExternalStorageDirectoryPath+`/MoLietSi_KhongXoa/${tomb.id}_${imageType}.jpg`;
            await RNFS.mkdir(RNFS.ExternalStorageDirectoryPath+"/MoLietSi_KhongXoa").then(d => console.log(d)).catch(e => console.log(e) );
            await RNFS.moveFile(Platform.OS === 'android' ? response.uri.replace("file://", "") : response.uri, savedUri)
              .then((success) => {
                savedUri = Platform.OS === 'android' ? "file://" + savedUri : savedUri;
              })
              .catch((err) => {
                savedUri = response.uri.slice();
                console.log(err.message);
                bugsnag.notify(err);
              });;
            console.log("responseUri, savedUri", response.uri, savedUri);
          }
          
          this.props.setCaptured(tomb.id, {
            ...tombImages,
            _new: tomb._new,
            photoMain: tomb.photoMain,
            photoPortrait: tomb.photoPortrait,
            photoLandscape: tomb.photoLandscape,
            [imageType]: {
              uri: savedUri,
              width: response.width,
              height: response.height,
            }
          });
          
          const photoMetas = tomb.photoMetas ? tomb.photoMetas.slice() : [{}, {}, {}];
          const photoMeta = {
            gps: {
              lat: position.lat,
              long: position.long,
            },
            time: new Date().toISOString()
          }
          if (imageType === 'main') {
            photoMetas[0] = photoMeta;
          } else if (imageType === 'portrait') {
            photoMetas[1] = photoMeta;
          } else if (imageType === 'landscape') {
            photoMetas[2] = photoMeta;
          }

          this.props[tomb._new ? "setNewTombs" : "setUpdateTombs"]([{
            ...tomb,
            photoMetas
          }]);

          if (!photoMetas[0] || !photoMetas[0].time) {
            this.onCaptureImage("main", true);
          } else if (!photoMetas[1] || !photoMetas[1].time) {
            this.onCaptureImage("portrait", true);
          } else if (!photoMetas[2] || !photoMetas[2].time) {
            this.onCaptureImage("landscape", true);
          }
        }
        
        setTimeout(() => {this.capturing = false;}, 800);
      });
    }
  }
  
  havePermission() {
    const tomb = this.getTomb();
    return this.props.profile.id === tomb.managedBy || !tomb.managedBy;
  }

  render() {
    const tomb = this.getTomb();
    const { note } = tomb;
    
    // console.log(tomb);
    
    const tags = [
      {
        name: 'Sai Liệt sĩ',
        value: 'wrongMartyr',
      },
      {
        name: 'Đúng Liệt sĩ, Sai thông tin',
        value: 'wrongProps',
      },
      {
        name: 'Bia Vô danh cần khắc lại bia',
        value: 'noName',
      },
      {
        name: 'Không có mộ',
        value: 'noTomb',
      },
    ];
    
    return (
      <View style={{flex: 1}}>
        <ScrollView style={{padding: 10, marginBottom: this.havePermission(tomb) ? 50 : 0, backgroundColor: '#f2f2f2', flex: 1}}>
          
          <View style={{padding: 15, borderColor: '#e2e2e2', borderWidth: 1, borderRadius: 5, backgroundColor: 'white', marginBottom: 10}}>
            <Text style={styles.text}>Liệt sĩ: <Text style={{fontWeight: 'bold'}}>{tomb.fullname}</Text></Text>
            <Text style={styles.text}>Nguyên quán: <Text style={{fontWeight: 'bold'}}>{`${tomb.town ? tomb.town + ", " : ""}${tomb.district ? tomb.district + ", " : ""}${tomb.province}`}</Text></Text>
            <Text style={styles.text}>Năm sinh: <Text style={{fontWeight: 'bold'}}>{`${tomb.dob}/${tomb.mob}/${tomb.yob}`}</Text></Text>
            <Text style={styles.text}>Năm hy sinh: <Text style={{fontWeight: 'bold'}}>{`${tomb.dod}/${tomb.mod}/${tomb.yod}`}</Text></Text>
            <Text style={styles.text}>Đơn vị: <Text style={{fontWeight: 'bold'}}>{tomb.unit}</Text></Text>
          </View>
          
          <View style={{flexDirection: 'row', marginBottom: 10, justifyContent: 'space-between'}}>
            <TouchableOpacity
              disabled={!this.havePermission() && !tomb.mainImage}
              onPress={() => this.onCaptureImage("main")}
              style={{...styles.image, ...styles.imageWrapper}}>
              {tomb.mainImage ? (
                <View>
                  <ImageBackground resizeMode="stretch" source={{ uri: tomb.mainImage.uri+(Platform.OS === 'android' && !tomb.mainImage.isSynced ? ('?random_number='+new Date().getTime()) : ''), width: tomb.width, height: tomb.height, cache: 'reload' }} style={styles.image}/>
                  {tomb.mainImage.isSynced ? (
                    <Icon name="ios-checkmark-circle" size={30} style={{position: 'absolute', top: 10, right: 10, color: 'green'}}/>
                  ) : (
                    <Icon name="ios-close-circle" size={30} style={{position: 'absolute', top: 10, right: 10, color: 'red'}}/>
                  )}
                </View>
              ) : (
                <View style={{alignItems: 'center'}}>
                  <Icon name="ios-image" size={50} />
                  <Text>Ảnh bia mộ</Text>
                </View>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              disabled={!this.havePermission() && !tomb.portraitImage}
              onPress={() => this.onCaptureImage("portrait")}
              style={{...styles.image, ...styles.imageWrapper}}>
              {tomb.portraitImage ? (
                <View>
                  <ImageBackground resizeMode="stretch" source={{ uri: tomb.portraitImage.uri+(Platform.OS === 'android' && !tomb.portraitImage.isSynced ? ('?random_number='+new Date().getTime()) : ''), width: tomb.width, height: tomb.height, cache: 'reload' }} style={styles.image} />
                  {tomb.portraitImage.isSynced ? (
                    <Icon name="ios-checkmark-circle" size={30} style={{position: 'absolute', top: 10, right: 10, color: 'green'}}/>
                  ) : (
                    <Icon name="ios-close-circle" size={30} style={{position: 'absolute', top: 10, right: 10, color: 'red'}}/>
                  )}
                </View>
              ) : (
                <View style={{alignItems: 'center'}}>
                  <Icon name="ios-image" size={50} />
                  <Text>Ảnh mặt trước mộ</Text>
                </View>
              )}
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            disabled={!this.havePermission() && !tomb.mainImage}
            onPress={() => this.onCaptureImage("landscape")}
            style={{...styles.imageWrapper, ...styles.landscapeImage}}>
            {tomb.landscapeImage ? (
              <View>
                <Image resizeMode="stretch" source={{ uri: tomb.landscapeImage.uri+(Platform.OS === 'android' && !tomb.landscapeImage.isSynced ? ('?random_number='+new Date().getTime()) : ''), width: tomb.width, height: tomb.height, cache: 'reload' }} style={styles.landscapeImage} />
                {tomb.landscapeImage.isSynced ? (
                  <Icon name="ios-checkmark-circle" size={30} style={{position: 'absolute', top: 10, right: 10, color: 'green'}}/>
                ) : (
                  <Icon name="ios-close-circle" size={30} style={{position: 'absolute', top: 10, right: 10, color: 'red'}}/>
                )}
              </View>
            ) : (
              <View style={{alignItems: 'center'}}>
                <Icon name="ios-image" size={50} />
                <Text>Ảnh toàn cảnh</Text>
              </View>
            )}
          </TouchableOpacity>
          
          {tags.map(tag => (
            <TouchableOpacity 
              key={tag.value}
              disabled={!this.havePermission()}
              onPress={() => this.onTagClick(tag.value)} 
              style={{
                flexDirection: 'row',
                alignItems: "center",
                padding: 8,
                backgroundColor: tomb[tag.value] ? 'orange' : 'white',
                marginTop: 10
              }}>
              {tomb[tag.value] ? (
                <Icon name="ios-checkbox-outline" size={35}/>
              ) : (
                <Icon name="ios-square-outline" size={35}/>
              )}
              <Text style={{marginLeft: 10, fontSize: 20, fontWeight: 'bold'}}>{tag.name}</Text>
            </TouchableOpacity>
          ))}
          
          <View style={{marginTop: 10, marginBottom: 50}}>
            <Text style={{marginBottom: 5, fontSize: 20, fontWeight: 'bold'}}>Ghi chú</Text>
            <TextInput
              style={{backgroundColor: 'white', height: 60, padding: 8}}
              multiline={true}
              numberOfLines={3}
              onChangeText={text => this.havePermission() ? this.onNoteChange(text) : null}
              value={note}
            />
          </View>
          
        </ScrollView>
        {this.havePermission() ? (
          <TouchableOpacity 
            onPress={() => this.props.goBack()}
            style={{
              height: 55,
              backgroundColor: '#4484ce',
              position: 'absolute',
              bottom: 0,
              left: 0,
              right: 0,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{
              color: 'white',
              fontWeight: 'bold',
              fontSize: 18
            }}>HOÀN THÀNH</Text>
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }
}

function bindAction(dispatch) {
  return {
    navigate: (route) => dispatch(navigate(route)),
    goBack: () => dispatch(goBack()),
    setNewTombs: (tombs) => dispatch(setNewTombs(tombs)),
    setUpdateTombs: (tombs) => dispatch(setUpdateTombs(tombs)),
    setCaptured: (tombId, images) => dispatch(setCaptured(tombId, images)),
  };
}

function mapStateToProps(state) {
  return {
    cdnUrl: state.user.config ? state.user.config.cdnUrl : DEFAULT_CDN_URL,
    profile: state.user.profile,
    newTombs: state.user.newTombs,
    updateTombs: state.user.updateTombs,
    captured: state.user.captured,
  };
}

export default connect(mapStateToProps, bindAction)(TombPage);
